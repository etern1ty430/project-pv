﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace WindowsFormsApp2
{
    class dijkstra
    {
        const int height = 15;
        const int width = 15;
        int[,] maze = new int[width, height];
        public int startX = 0, startY = 0; // Starting X and Y values
        public int endX = 9, endY = 9;     // Ending X and Y values
        List<List<int>> completePathX = new List<List<int>>();
        List<List<int>> completePathY = new List<List<int>>();
        // List of solutions of the maze

        public void InitMaze()
        {
            // Create Maze (0 = path, 1 = wall)
            int[,] maze1 = new int[,]{
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,0,0,0,0,0,0,0,0,0}
            };

            int[,] maze2 = new int[,]{
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1},
                {1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1},
                {1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
            };

            int[,] maze3 = new int[,]{
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,1,0,1,1,1,0,1,0,1},
                { 0,1,0,0,0,0,0,1,0,0},
                { 0,1,0,1,1,1,0,1,0,1},
                { 0,0,0,0,0,0,0,0,0,0},
                { 1,1,0,1,0,1,0,1,0,0},
                { 1,1,0,1,0,1,0,1,0,0},
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,0,1,0,0,0,1,0,0,0},
                { 0,0,0,0,0,0,0,0,0,0}
            };
            maze = maze2;
        }

        bool alreadyVisited(List<int> pX, List<int> pY, int x, int y)
        {
            bool notVisited = true;
            int n = pX.Count - 1;
            while (n >= 0 && notVisited)
            {
                if (pX[n] == x)
                    if (pY[n] == y)
                    {
                        notVisited = false;
                    }
                n--;
            }
            return !notVisited;
        }

        public void SolveMaze()
        {
            List<List<int>> pathXList = new List<List<int>>();
            List<List<int>> pathYList = new List<List<int>>();
            List<int> pathX = new List<int> { startX };
            List<int> pathY = new List<int> { startY };
            pathXList.Add(pathX);
            pathYList.Add(pathY);

            while (pathXList.Count > 0)
            {
                int pathNumber = pathXList.Count;
                while (pathNumber > 0)
                {
                    pathX = pathXList[0];
                    pathY = pathYList[0];
                    int n = pathX.Count - 1;
                    int x = pathX[n];
                    int y = pathY[n];
                    if (x == endX && y == endY)
                    {
                        completePathX.Add(pathX);
                        completePathY.Add(pathY);
                        pathXList.RemoveAt(0);
                        pathYList.RemoveAt(0);
                    }
                    else
                    {
                        if (x < width - 1) // Checks if not on right edge
                            if (maze[x + 1, y] != 1)
                                if (!alreadyVisited(pathX, pathY, x + 1, y))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x + 1);
                                    pY.Add(y);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        if (x > 0) // Checks if not on left edge
                            if (maze[x - 1, y] != 1)
                                if (!alreadyVisited(pathX, pathY, x - 1, y))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x - 1);
                                    pY.Add(y);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        if (y > 0)  // Checks if not on top edge
                            if (maze[x, y - 1] != 1)
                                if (!alreadyVisited(pathX, pathY, x, y - 1))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x);
                                    pY.Add(y - 1);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        if (y < height - 1) // Checks if not on bottom edge
                            if (maze[x, y + 1] != 1)
                                if (!alreadyVisited(pathX, pathY, x, y + 1))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x);
                                    pY.Add(y + 1);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        pathXList.RemoveAt(0);
                        pathYList.RemoveAt(0);
                    }
                    pathNumber--;
                }
            }
        }

        public void PrintOneMaze(List<int> pX, List<int> pY,bool save)
        {
            string hasil = "";
            string[] symbols = { "0", "#", "." };
            int[,] m = (int[,])maze.Clone(); //deep copy because it is an array of int
            int n = pX.Count;
            //The solution path with 2
            for (int i = 0; i < n; i++)
            {
                m[pX[i], pY[i]] = 2;
            }
            for (int row = 0; row < 2 * height + 2; row++)
                Console.Out.Write("-");
            Console.Out.WriteLine();
            for (int row = 0; row < width; row++)
            {
                Console.Out.Write("|");
                for (int col = 0; col < height; col++)
                {
                    Console.Out.Write(symbols[m[row, col]]);
                    hasil += symbols[m[row, col]];
                }
                Console.Out.Write("|");
                Console.Out.WriteLine();
                hasil += "\n";
            }
            for (int row = 0; row < 2 * height + 2; row++)
                Console.Out.Write("-");
            Console.Out.WriteLine();
            hasil += "\n";
            if (save)
            {
                StreamWriter sw;
                sw = new StreamWriter(Environment.CurrentDirectory + "/solve.txt");
                sw.Write(hasil);
                sw.Close();
            }
        }
        //djikstra untuk menyimpan semua kemungkinan jalan
        public void PrintMaze()
        {
            int n = completePathX.Count;
            for (int i = 0; i < n; i++)
            {
                PrintOneMaze(completePathX[i], completePathY[i],false);
                Console.Out.WriteLine();
            }
            string list_langkah = "";
            for (int i = 0; i < n; i++)
            {
                list_langkah += completePathX[i].Count+"\n";
            }
            StreamWriter sw;
            sw = new StreamWriter(Environment.CurrentDirectory + "/daftar_langkah.txt");
            sw.Write(list_langkah);
            sw.Close();
            Console.Out.WriteLine();
        }
        //min sudah di ekstrak dan di cari di path yang sesuai
        public void PrintMaze(int langkah)
        {
            int n = completePathX.Count;
            for (int i = 0; i < n; i++)
            {
                PrintOneMaze(completePathX[i], completePathY[i], false);
                Console.Out.WriteLine();
            }
            int min = 99;
            int index = -1;
            string list_langkah = "";
            for (int i = 0; i < n; i++)
            {
                if (completePathX[i].Count == langkah)
                {
                    min = completePathX[i].Count;
                    index = i;
                    break;
                }
            }
            Console.Out.WriteLine("\r\n" + n + " solutions");
            Console.WriteLine("solution ke : " + index + " & dengan banyak : " + min + " langkah");
            PrintOneMaze(completePathX[index], completePathY[index], true);
            Console.Out.WriteLine();
        }
    }
}
