﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class isi_map
    {
        private int _x;
        private int _y;
        private string _jenis;
        public isi_map(int x,int y,string jenis)
        {
            this._x = x;
            this._y = y;
            this._jenis = jenis;
        }
        public int x
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
            }
        }
        public int y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
            }
        }
        public string jenis
        {
            get
            {
                return _jenis;
            }
            set
            {
                _jenis = value;
            }
        }
    }
}
