﻿namespace WindowsFormsApp2
{
    partial class map
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.eagle_eye = new System.Windows.Forms.Timer(this.components);
            this.timermeledak = new System.Windows.Forms.Timer(this.components);
            this.timerledak = new System.Windows.Forms.Timer(this.components);
            this.mine_animation = new System.Windows.Forms.Timer(this.components);
            this.cek_win = new System.Windows.Forms.Timer(this.components);
            this.step_mine = new System.Windows.Forms.Timer(this.components);
            this.mine_meledak = new System.Windows.Forms.Timer(this.components);
            this.refresh_mana = new System.Windows.Forms.Timer(this.components);
            this.speed_up = new System.Windows.Forms.Timer(this.components);
            this.timer_mine_player = new System.Windows.Forms.Timer(this.components);
            this.timer_ledak_mine = new System.Windows.Forms.Timer(this.components);
            this.step_mine_player = new System.Windows.Forms.Timer(this.components);
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // eagle_eye
            // 
            this.eagle_eye.Enabled = true;
            this.eagle_eye.Interval = 1000;
            this.eagle_eye.Tick += new System.EventHandler(this.eagle_eye_Tick);
            // 
            // timermeledak
            // 
            this.timermeledak.Enabled = true;
            this.timermeledak.Interval = 500;
            this.timermeledak.Tick += new System.EventHandler(this.timermeledak_Tick);
            // 
            // timerledak
            // 
            this.timerledak.Enabled = true;
            this.timerledak.Interval = 250;
            this.timerledak.Tick += new System.EventHandler(this.timerledak_Tick);
            // 
            // mine_animation
            // 
            this.mine_animation.Enabled = true;
            this.mine_animation.Interval = 250;
            this.mine_animation.Tick += new System.EventHandler(this.mine_animation_Tick);
            // 
            // cek_win
            // 
            this.cek_win.Enabled = true;
            this.cek_win.Interval = 1;
            this.cek_win.Tick += new System.EventHandler(this.cek_win_Tick);
            // 
            // step_mine
            // 
            this.step_mine.Enabled = true;
            this.step_mine.Interval = 1;
            this.step_mine.Tick += new System.EventHandler(this.step_mine_Tick);
            // 
            // mine_meledak
            // 
            this.mine_meledak.Enabled = true;
            this.mine_meledak.Interval = 250;
            this.mine_meledak.Tick += new System.EventHandler(this.mine_meledak_Tick);
            // 
            // refresh_mana
            // 
            this.refresh_mana.Enabled = true;
            this.refresh_mana.Interval = 1000;
            this.refresh_mana.Tick += new System.EventHandler(this.refresh_mana_Tick);
            // 
            // speed_up
            // 
            this.speed_up.Enabled = true;
            this.speed_up.Interval = 1000;
            this.speed_up.Tick += new System.EventHandler(this.speed_up_Tick);
            // 
            // timer_mine_player
            // 
            this.timer_mine_player.Enabled = true;
            this.timer_mine_player.Interval = 200;
            this.timer_mine_player.Tick += new System.EventHandler(this.timer_mine_player_Tick);
            // 
            // timer_ledak_mine
            // 
            this.timer_ledak_mine.Enabled = true;
            this.timer_ledak_mine.Interval = 250;
            this.timer_ledak_mine.Tick += new System.EventHandler(this.timer_ledak_bomb_Tick);
            // 
            // step_mine_player
            // 
            this.step_mine_player.Enabled = true;
            this.step_mine_player.Interval = 1;
            this.step_mine_player.Tick += new System.EventHandler(this.step_mine_player_Tick);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::WindowsFormsApp2.Properties.Resources.aqua;
            this.pictureBox4.Location = new System.Drawing.Point(317, 22);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(56, 53);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::WindowsFormsApp2.Properties.Resources.yellow;
            this.pictureBox3.Location = new System.Drawing.Point(223, 22);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(56, 53);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WindowsFormsApp2.Properties.Resources.green;
            this.pictureBox2.Location = new System.Drawing.Point(130, 22);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(56, 53);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::WindowsFormsApp2.Properties.Resources.black;
            this.pictureBox1.Image = global::WindowsFormsApp2.Properties.Resources.black;
            this.pictureBox1.Location = new System.Drawing.Point(42, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 53);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(42, 92);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(56, 53);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new System.Drawing.Point(130, 92);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(56, 53);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(223, 92);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(56, 53);
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new System.Drawing.Point(317, 92);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(56, 53);
            this.pictureBox8.TabIndex = 7;
            this.pictureBox8.TabStop = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.button1.ForeColor = System.Drawing.Color.Snow;
            this.button1.Location = new System.Drawing.Point(42, 184);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 85);
            this.button1.TabIndex = 8;
            this.button1.Text = "STEP";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 641);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "map";
            this.Text = "map";
            this.Load += new System.EventHandler(this.forest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer eagle_eye;
        private System.Windows.Forms.Timer timermeledak;
        private System.Windows.Forms.Timer timerledak;
        private System.Windows.Forms.Timer mine_animation;
        private System.Windows.Forms.Timer cek_win;
        private System.Windows.Forms.Timer step_mine;
        private System.Windows.Forms.Timer mine_meledak;
        private System.Windows.Forms.Timer refresh_mana;
        private System.Windows.Forms.Timer speed_up;
        private System.Windows.Forms.Timer timer_mine_player;
        private System.Windows.Forms.Timer timer_ledak_mine;
        private System.Windows.Forms.Timer step_mine_player;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button button1;
    }
}