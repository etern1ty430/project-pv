﻿namespace WindowsFormsApp2
{
    partial class loadingscreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loadingscreen));
            this.pressbutton = new System.Windows.Forms.Button();
            this.loading_progress = new System.Windows.Forms.Timer(this.components);
            this.loading = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.loading)).BeginInit();
            this.SuspendLayout();
            // 
            // pressbutton
            // 
            this.pressbutton.Location = new System.Drawing.Point(752, 825);
            this.pressbutton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pressbutton.Name = "pressbutton";
            this.pressbutton.Size = new System.Drawing.Size(364, 76);
            this.pressbutton.TabIndex = 6;
            this.pressbutton.UseVisualStyleBackColor = true;
            this.pressbutton.Click += new System.EventHandler(this.pressbutton_Click);
            this.pressbutton.MouseLeave += new System.EventHandler(this.pressbutton_MouseLeave);
            this.pressbutton.MouseHover += new System.EventHandler(this.pressbutton_MouseHover);
            // 
            // loading_progress
            // 
            this.loading_progress.Tick += new System.EventHandler(this.loading_progress_Tick);
            // 
            // loading
            // 
            this.loading.Image = ((System.Drawing.Image)(resources.GetObject("loading.Image")));
            this.loading.Location = new System.Drawing.Point(752, 414);
            this.loading.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.loading.Name = "loading";
            this.loading.Size = new System.Drawing.Size(401, 346);
            this.loading.TabIndex = 8;
            this.loading.TabStop = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(501, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(836, 234);
            this.label1.TabIndex = 10;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // loadingscreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 922);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loading);
            this.Controls.Add(this.pressbutton);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "loadingscreen";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.loadingscreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.loading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button pressbutton;
        private System.Windows.Forms.Timer loading_progress;
        private System.Windows.Forms.PictureBox loading;
        private System.Windows.Forms.Label label1;
    }
}