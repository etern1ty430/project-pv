﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp2
{
    class FibHeapNode<T>
    {
        private bool c, mark;
        private int degree;
        private T key;
        private FibHeapNode<T> parent, child, left, right;

        public bool C
        {
            get
            {
                return c;
            }

            set
            {
                c = value;
            }
        }

        public bool Mark
        {
            get
            {
                return mark;
            }

            set
            {
                mark = value;
            }
        }

        public int Degree
        {
            get
            {
                return degree;
            }

            set
            {
                degree = value;
            }
        }

        public T Key
        {
            get
            {
                return key;
            }

            set
            {
                key = value;
            }
        }

        public FibHeapNode<T> Parent
        {
            get
            {
                return parent;
            }

            set
            {
                parent = value;
            }
        }

        public FibHeapNode<T> Left
        {
            get
            {
                return left;
            }

            set
            {
                left = value;
            }
        }

        public FibHeapNode<T> Right
        {
            get
            {
                return right;
            }

            set
            {
                right = value;
            }
        }

        public FibHeapNode<T> Child
        {
            get
            {
                return child;
            }

            set
            {
                child = value;
            }
        }

        public FibHeapNode() { }

        public FibHeapNode(int degree, T key)
        {
            this.degree = degree;
            this.key = key;
        }

        public FibHeapNode(FibHeapNode<T> copy)
        {
            this.key = copy.key;
            this.child = copy.child;
            this.left = copy.left;
            this.right = copy.right;
            this.parent = copy.parent;
        }
    }
}
