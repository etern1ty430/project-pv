﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp2
{
    class IntFibHeap
    {
        private int n;
        private FibHeapNode<int> min;

        public int N
        {
            get
            {
                return n;
            }
        }

        public FibHeapNode<int> Min
        {
            get
            {
                return min;
            }
        }

        public void Insert(ref IntFibHeap H, FibHeapNode<int> x)
        {
            x.Degree = 0;
            x.Parent = null;
            x.Child = null;
            x.Left = x;
            x.Right = x;
            x.Mark = false;
            x.C = false;

            if (H.min != null)
            {
                H.min.Left.Right = x;
                x.Right = H.min;
                x.Left = H.min.Left;
                H.min.Left = x;

                if (x.Key < H.min.Key)
                    H.min = x;
            }

            else
                H.min = x;

            H.n++;
        }

        public IntFibHeap Union(IntFibHeap H1, IntFibHeap H2)
        {

            FibHeapNode<int> np;
            IntFibHeap H = new IntFibHeap();

            H = H1;
            H.min.Left.Right = H2.min;
            H2.min.Left.Right = H.min;
            np = H.min.Left;
            H.min.Left = H2.min.Left;
            H2.min.Left = np;
            return H;
        }

        public void Link(ref IntFibHeap H, FibHeapNode<int> y,
            FibHeapNode<int> z)
        {
            y.Left.Right = y.Right;
            y.Right.Left = y.Left;

            if (z.Right == z)
                H.min = z;

            y.Left = y;
            y.Right = y;
            y.Parent = z;

            if (z.Child == null)
                z.Child = y;

            y.Right = z.Child;
            y.Left = z.Child.Left;
            z.Child.Left.Right = y;
            z.Child.Left = y;

            if (y.Key < z.Child.Key)
                z.Child = y;

            z.Degree++;
        }

        public void Consolidate(ref IntFibHeap H)
        {
            int d, D = 1 + (int)Math.Ceiling((Math.Log(H.n) / Math.Log(2.0)));
            FibHeapNode<int>[] A = new FibHeapNode<int>[D + 1];
            List<FibHeapNode<int>> list = new List<FibHeapNode<int>>();
            FibHeapNode<int> x = H.min, y, np, pt = x;

            do
            {
                pt = pt.Right;
                d = x.Degree;

                while (A[d] != null)
                {
                    y = A[d];

                    if (x.Key > y.Key)
                    {
                        np = x;
                        x = y;
                        y = np;
                    }

                    if (y == H.min)
                        H.min = x;

                    Link(ref H, y, x);

                    if (x.Right == x)
                        H.min = x;

                    A[d] = null;
                    d++;
                }

                A[d] = x;
                x = x.Right;
            } while (x != H.min);

            H = new IntFibHeap();

            for (int j = 0; j <= D; j++)
            {
                if (A[j] != null)
                {
                    A[j].Left = A[j];
                    A[j].Right = A[j];

                    if (H.min != null)
                    {
                        H.min.Left.Right = A[j];
                        A[j].Right = H.min;
                        A[j].Left = H.min.Left;
                        H.min.Left = A[j];
                        H.n++;

                        if (A[j].Key < H.min.Key)
                            H.min = A[j];
                    }

                    else
                    {
                        H.min = A[j];
                        H.n = 1;
                    }
                }
            }
        }

        public FibHeapNode<int> ExtractMin(ref IntFibHeap H)
        {
            FibHeapNode<int> ptr = H.min, z = H.min, x, np;

            if (z == null)
                return z;

            x = null;

            if (z.Child != null)
                x = z.Child;

            if (x != null)
            {
                ptr = x;

                do
                {
                    np = x.Right;
                    H.min.Left.Right = x;
                    x.Right = H.min;
                    x.Left = H.min.Left;
                    H.min.Left = x;

                    if (x.Key < H.min.Key)
                        H.min = x;

                    x.Parent = null;
                    x = np;
                } while (np != ptr);
            }

            z.Left.Right = z.Right;
            z.Right.Left = z.Left;
            H.min = z.Right;

            if (z == z.Right && z.Child == null)
                H.min = null;

            else
            {
                H.min = z.Right;

                if (H.n > 0)
                    Consolidate(ref H);
            }

            return z;
        }

        private void Cut(ref IntFibHeap H, FibHeapNode<int> x,
            FibHeapNode<int> y)
        {
            if (x == x.Right)
                y.Child = null;

            x.Left.Right = x.Right;
            x.Right.Left = x.Left;

            if (x == y.Child)
                y.Child = x.Right;

            y.Degree--;
            x.Right = x;
            x.Left = x;
            H.min.Left.Right = x;
            x.Right = H.min;
            x.Left = H.min.Left;
            H.min.Left = x;
            x.Parent = null;
            x.Mark = false;
        }

        private void CascadingCut(ref IntFibHeap H, FibHeapNode<int> y)
        {
            FibHeapNode<int> z = y.Parent;

            if (z != null)
            {
                if (y.Mark == false)
                    y.Mark = true;

                else
                {
                    Cut(ref H, y, z);
                    CascadingCut(ref H, z);
                }
            }
        }

        public FibHeapNode<int> Find(FibHeapNode<int> min, int k)
        {
            FibHeapNode<int> x = min, p = null;

            x.C = true;

            if (x.Key == k)
            {
                p = x;
                x.C = false;
                return p;

            }

            if (p == null)
            {
                if (x.Child != null)
                    p = Find(x.Child, k);

                if (x.Right.C != true)
                    p = Find(x.Right, k);

            }

            x.C = false;
            return p;
        }

        public bool DecreaseKey(ref IntFibHeap H, FibHeapNode<int> x, int k)
        {
            if (k > x.Key)
                return false;

            x.Key = k;

            FibHeapNode<int> y = x.Parent;

            if (y != null && x.Key < y.Key)
            {
                Cut(ref H, x, y);
                CascadingCut(ref H, y);
            }

            if (x.Key < H.min.Key)
                H.min = x;

            return true;
        }

        public void Delete(ref IntFibHeap H, FibHeapNode<int> x)
        {
            if (DecreaseKey(ref H, x, int.MinValue))
                ExtractMin(ref H);
        }
    }
}
