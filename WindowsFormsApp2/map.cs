﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Xml;
using WindowsFormsApp2;
using System.Xml.Serialization;
using System.IO;

namespace WindowsFormsApp2
{
    public partial class map : Form
    {
        public class inventory_bomb
        {
            public int horizontal;
            public int vertical;
            public inventory_bomb() { }
            public inventory_bomb(int horizontal, int vertical)
            {
                this.horizontal = horizontal;
                this.vertical = vertical;
            }
        }
        public class splash_bomb
        {
            public int ctrbomb = 0;
            public bool tanam = false;
            public bool meledak=false;
            public bool ledakan = false;
            public int ctrledak = 0;
            //mine
            public inventory_bomb isi_bomb=new inventory_bomb(0,0);
            public bool plantinmap = false;
            public Label label_unit;
            public List<Label> label_ledakan = new List<Label>();
            public splash_bomb() { }
            public splash_bomb(Label labelunit)
            {
                this.label_unit = labelunit;
            }
        }
        public class inventory_unit
        {
            public int uang = 500;
            public int mana_skill = 20;
            public int speed_up = 45;
            public int time_speed_up = 3;
            public int Movement_Speed = 75;
            public int time_planting = 200;
            public int timeEE = 1;
            public int jumlah_bomb = 1;
            public string skill = "";
            public string[] listUpgrade = new string[3];
            public string[] listBomb = new string[3];
            public int ctrlistUpgrade = 0;
            public int ctrlistBomb = 0;
            public string gloveShop = "-";
            public int level_skill=0;
            public inventory_unit() { }
        }
        public class unit
        {
            public inventory_unit bag = new inventory_unit();
            public splash_bomb mine = new splash_bomb(new Label());
            public int arah;
            public Timer animasi_gerak=new Timer();
            public Timer animasi_planting_bomb = new Timer();
            public Label label_unit;
            public List<splash_bomb> bomb=new List<splash_bomb>();
            public int ctrgerak = 0;
            public int ctrtimer = 0;
            public bool gerak = false;
            public bool use_skill = false;
            public bool planting_bomb = false;
            public ProgressBar mana=new ProgressBar();
            public Image[] atas = new Image[3];
            public Image[] bawah = new Image[3];
            public Image[] kanan = new Image[3];
            public Image[] kiri = new Image[3];
            public Image[] image_bomb = new Image[3];
            public unit() { }
            public unit(Label labelunit,int choose)
            {
                if (choose == 0)
                {
                    this.atas[0] = Image.FromFile("black\\a0.png");
                    this.atas[1] = Image.FromFile("black\\a1.png");
                    this.atas[2] = Image.FromFile("black\\a2.png");
                    this.bawah[0] = Image.FromFile("black\\b0.png");
                    this.bawah[1] = Image.FromFile("black\\b1.png");
                    this.bawah[2] = Image.FromFile("black\\b2.png");
                    this.kiri[0] = Image.FromFile("black\\kiri0.png");
                    this.kiri[1] = Image.FromFile("black\\kiri1.png");
                    this.kiri[2] = Image.FromFile("black\\kiri2.png");
                    this.kanan[0] = Image.FromFile("black\\k0.png");
                    this.kanan[1] = Image.FromFile("black\\k1.png");
                    this.kanan[2] = Image.FromFile("black\\k2.png");
                    this.image_bomb[0] = Image.FromFile("black\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("black\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("black\\bombawah.png");
                }
                else if (choose == 1)
                {
                    this.atas[0] = Image.FromFile("green\\a0.png");
                    this.atas[1] = Image.FromFile("green\\a1.png");
                    this.atas[2] = Image.FromFile("green\\a2.png");
                    this.bawah[0] = Image.FromFile("green\\b0.png");
                    this.bawah[1] = Image.FromFile("green\\b1.png");
                    this.bawah[2] = Image.FromFile("green\\b2.png");
                    this.kiri[0] = Image.FromFile("green\\kiri0.png");
                    this.kiri[1] = Image.FromFile("green\\kiri1.png");
                    this.kiri[2] = Image.FromFile("green\\kiri2.png");
                    this.kanan[0] = Image.FromFile("green\\k0.png");
                    this.kanan[1] = Image.FromFile("green\\k1.png");
                    this.kanan[2] = Image.FromFile("green\\k2.png");
                    this.image_bomb[0] = Image.FromFile("green\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("green\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("green\\bombawah.png");
                }
                else if (choose == 2)
                {
                    this.atas[0] = Image.FromFile("yellow\\a0.png");
                    this.atas[1] = Image.FromFile("yellow\\a1.png");
                    this.atas[2] = Image.FromFile("yellow\\a2.png");
                    this.bawah[0] = Image.FromFile("yellow\\b0.png");
                    this.bawah[1] = Image.FromFile("yellow\\b1.png");
                    this.bawah[2] = Image.FromFile("yellow\\b2.png");
                    this.kiri[0] = Image.FromFile("yellow\\kiri0.png");
                    this.kiri[1] = Image.FromFile("yellow\\kiri1.png");
                    this.kiri[2] = Image.FromFile("yellow\\kiri2.png");
                    this.kanan[0] = Image.FromFile("yellow\\k0.png");
                    this.kanan[1] = Image.FromFile("yellow\\k1.png");
                    this.kanan[2] = Image.FromFile("yellow\\k2.png");
                    this.image_bomb[0] = Image.FromFile("yellow\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("yellow\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("yellow\\bombawah.png");
                }
                else if (choose == 3)
                {
                    this.atas[0] = Image.FromFile("blue\\a0.png");
                    this.atas[1] = Image.FromFile("blue\\a1.png");
                    this.atas[2] = Image.FromFile("blue\\a2.png");
                    this.bawah[0] = Image.FromFile("blue\\b0.png");
                    this.bawah[1] = Image.FromFile("blue\\b1.png");
                    this.bawah[2] = Image.FromFile("blue\\b2.png");
                    this.kiri[0] = Image.FromFile("blue\\kiri0.png");
                    this.kiri[1] = Image.FromFile("blue\\kiri1.png");
                    this.kiri[2] = Image.FromFile("blue\\kiri2.png");
                    this.kanan[0] = Image.FromFile("blue\\k0.png");
                    this.kanan[1] = Image.FromFile("blue\\k1.png");
                    this.kanan[2] = Image.FromFile("blue\\k2.png");
                    this.image_bomb[0] = Image.FromFile("blue\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("blue\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("blue\\bombawah.png");
                }
                this.animasi_gerak.Tick += new EventHandler(animasi);
                this.animasi_planting_bomb.Tick += new EventHandler(timer_plant_bomb);
                this.arah = 0;
                this.label_unit= labelunit;
                this.mana.Minimum = 0;
                this.mana.Maximum = 100;
                this.mana.Size = new Size(50,10);
                this.mana.Value = 100;
                this.mana.Style = ProgressBarStyle.Continuous;
                this.mana.ForeColor = Color.Turquoise;
            }
            public void animasi(object sender, EventArgs e)
            {
                if (ctrgerak < 5 && gerak == true)
                {
                    animasi_gerak.Interval = bag.Movement_Speed;
                    if (arah == 1)
                    {
                        label_unit.Image = atas[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 10);
                        mana.Location = new Point(mana.Location.X, mana.Location.Y - 10);
                    }
                    else if (arah == 2)
                    {
                        label_unit.Image = bawah[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y + 10);
                        mana.Location = new Point(mana.Location.X, mana.Location.Y + 10);
                    }
                    else if (arah == 3)
                    {
                        label_unit.Image = kiri[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X - 10,label_unit.Location.Y);
                        mana.Location = new Point(mana.Location.X-10, mana.Location.Y);
                    }
                    else if (arah == 4)
                    {
                        label_unit.Image = kanan[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X + 10, label_unit.Location.Y);
                        mana.Location = new Point(mana.Location.X + 10, mana.Location.Y);
                    }
                    ctrgerak++;
                }
                else
                {
                    gerak = false;
                    ctrgerak = 0;
                }
            }
            public void timer_plant_bomb(object sender, EventArgs e)
            {
                if (planting_bomb == true)
                {
                    animasi_planting_bomb.Interval = bag.time_planting;
                    int index = -1;
                    for (int j = 0; j < bomb.Count; j++)
                    {
                        if (bomb[j].ctrbomb < 3 && bomb[j].tanam == true)
                        {
                            label_unit.Image = image_bomb[bomb[j].ctrbomb];
                            if (bomb[j].ctrbomb == 0)
                            {
                                bomb[j].label_unit.Image = new Bitmap("bomb1.png");
                                bomb[j].label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y + 50);
                                bomb[j].label_unit.BringToFront();
                            }
                            else if (bomb[j].ctrbomb == 1)
                            {
                                bomb[j].label_unit.Image = new Bitmap("bomb1.png");
                                bomb[j].label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 50);
                            }
                            else
                            {
                                bomb[j].label_unit.Image = new Bitmap("bomb1.png");
                                bomb[j].label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y);
                                label_unit.BringToFront();
                            }
                            bomb[j].ctrbomb++;
                        }
                        else if (bomb[j].ctrbomb == 3 && bomb[j].tanam == true)
                        {
                            planting_bomb = false;
                            bomb[j].tanam = false;
                            bomb[j].ctrbomb = 0;
                            bomb[j].meledak = true;
                            gerak = false;
                        }
                    }
                }
            }
        }
        public class obstacle
        {
            public Label label_unit;
            public string jenis;
            //ctr dan stepped untuk mine
            public int ctr = 0;
            public bool stepped = false;
            public obstacle() { }
            public obstacle(Label labelunit,string jenis)
            {
                this.jenis = jenis;
                this.label_unit = labelunit;
            }
        }
        //start program
        int animasi_mine = 0;
        Random rand = new Random();
        List<obstacle> list_isi_map = new List<obstacle>();
        int choosed_map;
        public unit[] arr_unit = new unit[4];
        public string[] arr_save = new string[4];
        public string[] arr_mine = new string[4];
        public string[] arr_bomb = new string[4];
        PictureBox lantai = new PictureBox();

        public map()
        {
            InitializeComponent();
            lantai.Size = new Size(750,750);
            lantai.Location = new Point(0,0);
        }
        
        private void forest_Load(object sender, EventArgs e)
        {
            pictureBox1.Location = new Point(450+300, 900);
            pictureBox2.Location = new Point(550+ 300, 900);
            pictureBox3.Location = new Point(650 + 300, 900);
            pictureBox4.Location = new Point(750 + 300, 900);
            pictureBox5.Location = new Point(450 + 300, 1000);
            pictureBox6.Location = new Point(550 + 300, 1000);
            pictureBox7.Location = new Point(650 + 300, 1000);
            pictureBox8.Location = new Point(750 + 300, 1000);
            Make_Map();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            for (int i = 0; i < arr_unit.Length; i++)
            {
                arr_unit[i] = new unit(new Label(), i);
                for (int j = 0; j < arr_unit[i].bag.jumlah_bomb; j++)
                {
                    arr_unit[i].bomb.Add(new splash_bomb(new Label()));
                    arr_unit[i].bomb[j].isi_bomb.horizontal = 1;
                    arr_unit[i].bomb[j].isi_bomb.vertical = 1;
                }
                arr_unit[i].label_unit.Image = arr_unit[i].bawah[0];
                if (i==0)
                {
                    arr_unit[i].label_unit.Location = new Point(50, 50);
                }
                else if (i==1)
                {
                    arr_unit[i].label_unit.Location = new Point(600, 50);
                }
                else if (i == 2)
                {
                    arr_unit[i].label_unit.Location = new Point(50, 650);
                }
                else if (i == 3)
                {
                    arr_unit[i].label_unit.Location = new Point(600, 650);
                }
                arr_unit[i].label_unit.Size = new Size(50, 50);

                for (int j = 0; j < arr_unit[i].bomb.Count; j++)
                {
                    this.Controls.Add(arr_unit[i].bomb[j].label_unit);
                    arr_unit[i].bomb[j].label_unit.BackColor = Color.Transparent;
                    arr_unit[i].bomb[j].label_unit.FlatStyle = FlatStyle.Flat;
                    arr_unit[i].bomb[j].label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y - 20);
                    arr_unit[i].bomb[j].label_unit.Parent = lantai;
                    arr_unit[i].bomb[j].label_unit.Size = new Size(50, 50);
                }
                arr_unit[i].mine.label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y - 20);
                arr_unit[i].mine.label_unit.Image = new Bitmap("potato10.png");
                arr_unit[i].mine.label_unit.Size = new Size(50, 50);
                arr_unit[i].mine.isi_bomb.vertical = 0;
                arr_unit[i].mine.isi_bomb.horizontal = 0;
                this.Controls.Add(arr_unit[i].label_unit);
                this.Controls.Add(arr_unit[i].mana);
                arr_unit[i].mana.Location = new Point(arr_unit[i].label_unit.Location.X+450, arr_unit[i].label_unit.Location.Y - 10+150);
                arr_unit[i].mana.BringToFront();
                arr_unit[i].label_unit.BackColor = Color.Transparent;
                arr_unit[i].label_unit.FlatStyle = FlatStyle.Flat;
                arr_unit[i].label_unit.Parent = lantai;
                arr_unit[i].label_unit.BringToFront();
                arr_unit[i].animasi_gerak.Start();
                arr_unit[i].animasi_planting_bomb.Start();
                //isi skill
                arr_unit[i].bag.mana_skill = 20;
                int random = rand.Next(0,101);
                if (random<=45)
                {
                    arr_unit[i].bag.skill = "speed up";
                }
                else if (random <= 80)
                {
                    arr_unit[i].bag.skill = "eagle eye";
                }
                else if (random <= 100)
                {
                    arr_unit[i].bag.skill = "mine_bomb";
                }
            }
            this.speed_up.Start();
            //load 
            //player
            arr_save[0] = "saveplayer1.xml";
            arr_save[1] = "saveplayer2.xml";
            arr_save[2] = "saveplayer3.xml";
            arr_save[3] = "saveplayer4.xml";
            //mine
            arr_mine[0] = "saveplayer1mine.xml";
            arr_mine[1] = "saveplayer2mine.xml";
            arr_mine[2] = "saveplayer3mine.xml";
            arr_mine[3] = "saveplayer4mine.xml";
            //bomb
            arr_bomb[0] = "saveplayer1bomb.xml";
            arr_bomb[1] = "saveplayer2bomb.xml";
            arr_bomb[2] = "saveplayer3bomb.xml";
            arr_bomb[3] = "saveplayer4bomb.xml";
            int temp_halaman = ((Form1)MdiParent).save_or_load;
            //halaman save
            if (temp_halaman == 0)
            {
                int choosedplay = ((Form1)MdiParent).menu_select_player.chooseplayer;
                for (int i = 0; i < 4; i++)
                {
                    if (choosedplay == i)
                    {
                        //save inventory
                        int player = ((Form1)MdiParent).menu_select_player.chooseplayer;
                        System.IO.File.WriteAllText(arr_save[i], string.Empty);
                        Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_save[i]);
                        XmlSerializer xmlser = new XmlSerializer(typeof(inventory_unit));
                        xmlser.Serialize(stream, arr_unit[player].bag);
                        stream.Close();
                        //save mine
                        System.IO.File.WriteAllText(arr_mine[i], string.Empty);
                        Stream stream1 = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_mine[i]);
                        XmlSerializer xmlser1 = new XmlSerializer(typeof(inventory_bomb));
                        xmlser1.Serialize(stream1, arr_unit[player].mine.isi_bomb);
                        stream1.Close();
                        //save bomb
                        System.IO.File.WriteAllText(arr_bomb[i], string.Empty);
                        Stream stream2 = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_bomb[i]);
                        XmlSerializer xmlser2 = new XmlSerializer(typeof(inventory_bomb));
                        xmlser2.Serialize(stream2, arr_unit[player].bomb[0].isi_bomb);
                        stream2.Close();
                    }
                    else
                    {
                        //load inventory
                        XmlSerializer serializer = new XmlSerializer(typeof(inventory_unit));
                        using (FileStream stream = File.OpenRead(arr_save[i]))
                        {
                            arr_unit[i].bag = (inventory_unit)serializer.Deserialize(stream);
                        }
                        //load mine
                        XmlSerializer serializer1 = new XmlSerializer(typeof(inventory_bomb));
                        using (FileStream stream = File.OpenRead(arr_mine[i]))
                        {
                            arr_unit[i].mine.isi_bomb = (inventory_bomb)serializer1.Deserialize(stream);
                        }
                        //load bomb
                        XmlSerializer serializer2 = new XmlSerializer(typeof(inventory_bomb));
                        using (FileStream stream = File.OpenRead(arr_bomb[i]))
                        {
                            arr_unit[i].bomb.Clear();
                            for (int j = 0; j < arr_unit[i].bag.jumlah_bomb; j++)
                            {
                                arr_unit[i].bomb.Add(new splash_bomb(new Label()));
                                arr_unit[i].bomb[j].label_unit.BackColor = Color.Transparent;
                                arr_unit[i].bomb[j].label_unit.FlatStyle = FlatStyle.Flat;
                                arr_unit[i].bomb[j].label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y - 20);
                                arr_unit[i].bomb[j].label_unit.Parent = lantai;
                                arr_unit[i].bomb[j].label_unit.Size = new Size(50, 50);
                            }
                            inventory_bomb temp = (inventory_bomb)serializer2.Deserialize(stream);
                            foreach (splash_bomb x in arr_unit[i].bomb)
                            {
                                x.isi_bomb.horizontal = temp.horizontal;
                                x.isi_bomb.vertical = temp.vertical;
                                x.label_unit.Image = new Bitmap("bomb1.png");
                            }
                        }
                    }
                }
            }
            //halaman load
            else if (temp_halaman == 1)
            {
                for (int i = 0; i < 4; i++)
                {
                    //load inventory
                    XmlSerializer serializer = new XmlSerializer(typeof(inventory_unit));
                    using (FileStream stream = File.OpenRead(arr_save[i]))
                    {
                        arr_unit[i].bag = (inventory_unit)serializer.Deserialize(stream);
                    }
                    //load mine
                    XmlSerializer serializer1 = new XmlSerializer(typeof(inventory_bomb));
                    using (FileStream stream = File.OpenRead(arr_mine[i]))
                    {
                        arr_unit[i].mine.isi_bomb = (inventory_bomb)serializer1.Deserialize(stream);
                    }
                    //load bomb
                    XmlSerializer serializer2 = new XmlSerializer(typeof(inventory_bomb));
                    using (FileStream stream = File.OpenRead(arr_bomb[i]))
                    {
                        arr_unit[i].bomb.Clear();
                        for (int j = 0; j < arr_unit[i].bag.jumlah_bomb; j++)
                        {
                            arr_unit[i].bomb.Add(new splash_bomb(new Label()));
                            arr_unit[i].bomb[j].label_unit.BackColor = Color.Transparent;
                            arr_unit[i].bomb[j].label_unit.FlatStyle = FlatStyle.Flat;
                            arr_unit[i].bomb[j].label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y - 20);
                            arr_unit[i].bomb[j].label_unit.Parent = lantai;
                            arr_unit[i].bomb[j].label_unit.Size = new Size(50, 50);
                        }
                        inventory_bomb temp = (inventory_bomb)serializer2.Deserialize(stream);
                        foreach (splash_bomb x in arr_unit[i].bomb)
                        {
                            x.isi_bomb.horizontal = temp.horizontal;
                            x.isi_bomb.vertical = temp.vertical;
                            x.label_unit.Image = new Bitmap("bomb1.png");
                        }
                    }
                }
            }
            for (int i = 0; i < 4; i++)
            {
                if (i==0)
                {
                    if (arr_unit[i].bag.skill=="speed up")
                    {
                        pictureBox5.BackgroundImage = new Bitmap("skill_speedUp.png");
                    } 
                    else if (arr_unit[i].bag.skill == "eagle eye")
                    {
                        pictureBox5.BackgroundImage = new Bitmap("skill_eagleeye.png");
                    }
                    else if (arr_unit[i].bag.skill == "mine_bomb")
                    {
                        pictureBox5.BackgroundImage = new Bitmap("skill_bomb.png");
                    }
                    pictureBox5.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else if (i == 1)
                {
                    if (arr_unit[i].bag.skill == "speed up")
                    {
                        pictureBox6.BackgroundImage = new Bitmap("skill_speedUp.png");
                    }
                    else if (arr_unit[i].bag.skill == "eagle eye")
                    {
                        pictureBox6.BackgroundImage = new Bitmap("skill_eagleeye.png");
                    }
                    else if (arr_unit[i].bag.skill == "mine_bomb")
                    {
                        pictureBox6.BackgroundImage = new Bitmap("skill_bomb.png");
                    }
                    pictureBox6.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else if (i == 2)
                {
                    if (arr_unit[i].bag.skill == "speed up")
                    {
                        pictureBox7.BackgroundImage = new Bitmap("skill_speedUp.png");
                    }
                    else if (arr_unit[i].bag.skill == "eagle eye")
                    {
                        pictureBox7.BackgroundImage = new Bitmap("skill_eagleeye.png");
                    }
                    else if (arr_unit[i].bag.skill == "mine_bomb")
                    {
                        pictureBox7.BackgroundImage = new Bitmap("skill_bomb.png");
                    }
                    pictureBox7.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else if (i == 3)
                {
                    if (arr_unit[i].bag.skill == "speed up")
                    {
                        pictureBox8.BackgroundImage = new Bitmap("skill_speedUp.png");
                    }
                    else if (arr_unit[i].bag.skill == "eagle eye")
                    {
                        pictureBox8.BackgroundImage = new Bitmap("skill_eagleeye.png");
                    }
                    else if (arr_unit[i].bag.skill == "mine_bomb")
                    {
                        pictureBox8.BackgroundImage = new Bitmap("skill_bomb.png");
                    }
                    pictureBox8.BackgroundImageLayout = ImageLayout.Stretch;
                }
            }
        }
        
        public void Make_Map()
        {
            choosed_map = ((Form1)MdiParent).menu_select_map.map;
            ((Form1)MdiParent).menu_select_map.list_isi_map.Clear();
            if (choosed_map==0)
            {
                lantai.Image = new Bitmap("map1.png");
                lantai.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (choosed_map == 1)
            {
                lantai.Image = new Bitmap("map2.png");
                lantai.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (choosed_map == 2)
            {
                lantai.Image = new Bitmap("map3.png");
                lantai.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (choosed_map == 3)
            {
                lantai.Image = new Bitmap("map4.png");
                lantai.BackgroundImageLayout = ImageLayout.Stretch;
            }
            if (choosed_map<3)
            {
                string file = ((Form1)MdiParent).menu_select_map.file_map[((Form1)MdiParent).menu_select_map.map];
                int i = 0;
                int j = 0;
                string[] lines = lines = System.IO.File.ReadAllLines(@"" + file);
                foreach (string line in lines)
                {
                    string[] word = line.Split('-');
                    foreach (string karakter in word)
                    {
                        if (karakter == "2")
                        {
                            obstacle x = new obstacle(new Label(),"tembok");
                            x.label_unit.Location = new Point(j*50,i*50);
                            x.label_unit.Size = new Size(50,50);
                            if (choosed_map==0)
                            {
                                x.label_unit.Image = new Bitmap("forest_rock.png");
                            }
                            else if (choosed_map==1)
                            {
                                x.label_unit.Image = new Bitmap("sea_rock.png");
                            }
                            else if (choosed_map == 2)
                            {
                                x.label_unit.Image = new Bitmap("dragon_wall.png");
                            }
                            list_isi_map.Add(x);
                        }
                        else if (karakter == "1")
                        {
                            obstacle x = new obstacle(new Label(), "papan");
                            x.label_unit.Location = new Point(j * 50, i * 50);
                            x.label_unit.Size = new Size(50, 50);
                            if (choosed_map == 0)
                            {
                                x.label_unit.Image = new Bitmap("forest_tree.png");
                            }
                            else if (choosed_map == 1)
                            {
                                x.label_unit.Image = new Bitmap("sea_box.png");
                            }
                            else if (choosed_map == 2)
                            {
                                x.label_unit.Image = new Bitmap("dragon_rock.png");
                            }
                            int random = rand.Next(0, 101);
                            if (random<=50)
                            {
                                obstacle y = new obstacle(new Label(), "mine");
                                y.label_unit.Location = new Point(j * 50 , i * 50);
                                y.label_unit.Size = new Size(50, 50);
                                y.label_unit.Image = new Bitmap("potato00.png");
                                list_isi_map.Add(y);
                            }
                            list_isi_map.Add(x);
                        }
                        else
                        {
                            obstacle x = new obstacle(new Label(), "lantai");
                            x.label_unit.Location = new Point(j * 50, i * 50);
                            x.label_unit.Size = new Size(50, 50);
                            list_isi_map.Add(x);
                        }
                        j++;
                    }
                    j = 0;
                    i++;
                }
                foreach (obstacle x in list_isi_map)
                {
                    if (x.jenis == "lantai")
                    {
                        x.label_unit.Click += new EventHandler(click);
                    }
                    this.Controls.Add(x.label_unit);
                    if (x.jenis=="mine")
                    {
                        x.label_unit.BackColor = Color.Transparent;
                        x.label_unit.FlatStyle = FlatStyle.Flat;
                        x.label_unit.Parent = lantai;
                    }
                    else
                    {
                        x.label_unit.BackColor = Color.Transparent;
                        x.label_unit.FlatStyle = FlatStyle.Flat;
                        x.label_unit.Parent = lantai;
                        x.label_unit.BringToFront();
                    }
                }
                this.Controls.Add(lantai);
                lantai.Location = new Point(450, 150);
            }
            else
            {
                XmlDocument xmlDoc = new XmlDocument();
                if (File.Exists(Application.StartupPath + "/arena.xml"))
                {
                    xmlDoc.Load(Application.StartupPath + "/arena.xml");
                    XmlNode listNode = xmlDoc.DocumentElement;
                    if (listNode.Name == "list-map")
                    {
                        XmlNodeList listMap = listNode.ChildNodes;
                            XmlNode mapNode = listMap[0];
                            string[,] temp = new string[15, 20];
                            int a = 0;
                            int z = 0;
                            for (int j = 0; j < mapNode.InnerText.Length; j++)
                            {
                                if (mapNode.InnerText[j] == '|')
                                {
                                    a++;
                                    z = 0;
                                }
                                else
                                {
                                    temp[a, z] = mapNode.InnerText[j].ToString();
                                    z++;
                                }
                            }
                            for (int j = 0; j < 15; j++)
                            {
                                for (int k = 0; k < 20; k++)
                                {
                                    if (temp[j,k]=="0")
                                    {
                                        obstacle x = new obstacle(new Label(), "tembok");
                                        x.label_unit.Location = new Point(k * 50, j * 50);
                                        x.label_unit.Size = new Size(50, 50);
                                        x.label_unit.Image = new Bitmap("wall0.png");
                                        list_isi_map.Add(x);
                                    }
                                    else if (temp[j, k] == "1")
                                    {
                                        obstacle x = new obstacle(new Label(), "papan");
                                        x.label_unit.Location = new Point(k * 50, j * 50);
                                        x.label_unit.Size = new Size(50, 50);
                                        x.label_unit.Image = new Bitmap("wall1.png");
                                        int random = rand.Next(0, 101);
                                        if (random <= 50)
                                        {
                                            obstacle y = new obstacle(new Label(), "mine");
                                            y.label_unit.Location = new Point(k * 50, j * 50);
                                            y.label_unit.Size = new Size(50, 50);
                                            y.label_unit.Image = new Bitmap("potato00.png");
                                            list_isi_map.Add(y);
                                        }
                                        list_isi_map.Add(x);
                                    }
                                else if (temp[j, k] == "7")
                                {
                                    obstacle x = new obstacle(new Label(), "mine");
                                    x.label_unit.Location = new Point(k * 50, j * 50);
                                    x.label_unit.Size = new Size(50, 50);
                                    x.label_unit.Image = new Bitmap("wall1.png");
                                    int random = rand.Next(0, 101);
                                    if (random <= 50)
                                    {
                                        obstacle y = new obstacle(new Label(), "mine");
                                        y.label_unit.Location = new Point(k * 50, j * 50);
                                        y.label_unit.Size = new Size(50, 50);
                                        y.label_unit.Image = new Bitmap("potato00.png");
                                        list_isi_map.Add(y);
                                    }
                                    list_isi_map.Add(x);
                                }
                            }
                            }
                    }
                }
                foreach (obstacle x in list_isi_map)
                {
                    this.Controls.Add(x.label_unit);
                    if (x.jenis == "mine")
                    {
                        x.label_unit.BackColor = Color.Transparent;
                        x.label_unit.FlatStyle = FlatStyle.Flat;
                        x.label_unit.Parent = lantai;
                    }
                    else
                    {
                        x.label_unit.BackColor = Color.Transparent;
                        x.label_unit.FlatStyle = FlatStyle.Flat;
                        x.label_unit.Parent = lantai;
                        x.label_unit.BringToFront();
                    }
                }
                //lantai.Location = new Point(500);
                this.Controls.Add(lantai);
                lantai.Location = new Point(450, 150);
            }
        }
        public void click(object sender, EventArgs e)
        {
            foreach (obstacle x in list_isi_map)
            {
                if (x.jenis == "choosen")
                {
                    x.jenis = "lantai";
                }
            }
            Label temp = (Label)sender;
            dijkstra m = new dijkstra();
            m.startX = arr_unit[0].label_unit.Location.Y / 50;
            m.startY = arr_unit[0].label_unit.Location.X / 50;
            m.endX = temp.Location.Y / 50;
            m.endY = temp.Location.X / 50;
            m.InitMaze();
            m.SolveMaze();
            m.PrintMaze();
            //load
            IntFibHeap heap = new IntFibHeap();
            IntFibHeap H = new IntFibHeap();
            int i = 0;
            int j = 0;
            string[] linesx = new string[0];
            linesx = System.IO.File.ReadAllLines(@"daftar_langkah.txt");
            foreach (string line in linesx)
            {
                heap.Insert(ref H, new FibHeapNode<int>(0, Convert.ToInt16(line)));
            }
            FibHeapNode<int> min = heap.ExtractMin(ref H);
            int langkah_min = min.Key;//langkah minimal
            m.PrintMaze(langkah_min);
            string[] lines = new string[0];
            lines = System.IO.File.ReadAllLines(@"solve.txt");
            foreach (string line in lines)
            {
                foreach (char alphabets in line)
                {
                    if (alphabets == '.')
                    {
                        foreach (obstacle x in list_isi_map)
                        {
                            if (x.label_unit.Location.X == j * 50 && x.label_unit.Location.Y == i * 50)
                            {
                                x.jenis = "choosen";
                            }
                        }
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            MessageBox.Show("path telah terbentuk");
        }
        public void atas(int player)
        {
            bool cek = true;
            foreach (obstacle x in list_isi_map)
            {
                if (x.jenis == "tembok" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y - 50)
                {
                    cek = false;
                }
                else if (x.jenis == "papan" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y - 50)
                {
                    cek = false;
                }
            }
            if (cek == true && arr_unit[player].gerak == false && arr_unit[player].planting_bomb == false && arr_unit[player].label_unit.Image!=null)
            {
                arr_unit[player].arah = 1;
                arr_unit[player].ctrgerak = 0;
                arr_unit[player].gerak = true;
            }
            else if(cek == false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].label_unit.Image = arr_unit[player].atas[0];
            }
        }
        public void bawah(int player)
        {
            bool cek = true;
            foreach (obstacle x in list_isi_map)
            {
                if (x.jenis == "tembok" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y + 50)
                {
                    cek = false;
                }
                else if (x.jenis == "papan" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y + 50)
                {
                    cek = false;
                }
            }
            if (cek == true && arr_unit[player].gerak == false && arr_unit[player].planting_bomb == false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].arah = 2;
                arr_unit[player].ctrgerak = 0;
                arr_unit[player].gerak = true;
            }
            else if (cek == false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].label_unit.Image = arr_unit[player].bawah[0];
            }
        }
        public void kiri(int player)
        {
            bool cek = true;
            foreach (obstacle x in list_isi_map)
            {
                if (x.jenis == "tembok" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X - 50 && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y)
                {
                    cek = false;
                }
                else if (x.jenis == "papan" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X - 50 && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y)
                {
                    cek = false;
                }
            }
            if (cek == true && arr_unit[player].gerak == false && arr_unit[player].planting_bomb == false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].arah = 3;
                arr_unit[player].ctrgerak = 0;
                arr_unit[player].gerak = true;
            }
            else if (cek == false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].label_unit.Image = arr_unit[player].kiri[0];
            }
        }
        public void kanan(int player)
        {
            bool cek = true;
            foreach (obstacle x in list_isi_map)
            {
                if (x.jenis == "tembok" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X + 50 && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y)
                {
                    cek = false;
                }
                else if (x.jenis == "papan" && x.label_unit.Location.X == arr_unit[player].label_unit.Location.X + 50 && x.label_unit.Location.Y == arr_unit[player].label_unit.Location.Y)
                {
                    cek = false;
                }
            }
            if (cek == true && arr_unit[player].gerak == false && arr_unit[player].planting_bomb== false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].arah = 4;
                arr_unit[player].ctrgerak = 0;
                arr_unit[player].gerak = true;
            }
            else if (cek == false && arr_unit[player].label_unit.Image != null)
            {
                arr_unit[player].label_unit.Image = arr_unit[player].kanan[0];
            }
        }
        public void Plant_Bomb(int player)
        {
            bool cek = true;
            int index = -1;
            if (arr_unit[player].gerak == false && arr_unit[player].planting_bomb == false)
            {
                for (int i = 0; i < arr_unit[player].bomb.Count; i++)
                {
                    if (arr_unit[player].bomb[i].meledak == true || arr_unit[player].bomb[i].ledakan == true)
                    {
                        cek = false;
                    }
                    else
                    {
                        cek = true;
                        index = i;
                        i = 1000;
                    }
                }
                if (cek== true && arr_unit[player].label_unit.Image != null)
                {
                    arr_unit[player].bomb[index].ctrbomb = 0;
                    arr_unit[player].bomb[index].tanam = true;
                    arr_unit[player].planting_bomb = true;
                    arr_unit[player].gerak = true;
                    arr_unit[player].ctrgerak = 5;
                }
            }
            
        }
        public void skill(int player)
        {
            if (arr_unit[player].use_skill == false && arr_unit[player].gerak == false && arr_unit[player].planting_bomb == false)
            {
                if (arr_unit[player].mana.Value - arr_unit[player].bag.mana_skill >= 0 && arr_unit[player].label_unit.Image != null)
                {
                    arr_unit[player].ctrtimer = 0;
                    arr_unit[player].mana.Value -= arr_unit[player].bag.mana_skill;
                    arr_unit[player].use_skill = true;
                    if (arr_unit[player].use_skill==true && arr_unit[player].bag.skill =="mine_bomb" && arr_unit[player].label_unit.Image != null)
                    {
                        arr_unit[player].mine.ctrbomb = 0;
                        arr_unit[player].mine.tanam = true;
                        arr_unit[player].planting_bomb = true;
                        arr_unit[player].gerak = true;
                        arr_unit[player].ctrgerak = 5;
                    }
                }
            }
        }

        public void save_player_stat()
        {
            for (int i = 0; i < 4; i++)
            {
                //save inventory
                System.IO.File.WriteAllText(arr_save[i], string.Empty);
                Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_save[i]);
                XmlSerializer xmlser = new XmlSerializer(typeof(inventory_unit));
                xmlser.Serialize(stream, arr_unit[i].bag);
                stream.Close();
                //save mine
                System.IO.File.WriteAllText(arr_mine[i], string.Empty);
                Stream stream1 = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_mine[i]);
                XmlSerializer xmlser1 = new XmlSerializer(typeof(inventory_bomb));
                xmlser1.Serialize(stream1, arr_unit[i].mine.isi_bomb);
                stream1.Close();
                //save bomb
                System.IO.File.WriteAllText(arr_bomb[i], string.Empty);
                Stream stream2 = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_bomb[i]);
                XmlSerializer xmlser2 = new XmlSerializer(typeof(inventory_bomb));
                xmlser2.Serialize(stream2, arr_unit[i].bomb[0].isi_bomb);
                stream2.Close();
            }
        }

        private void timermeledak_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Count(); i++)
            {
                for (int j = 0; j < arr_unit[i].bomb.Count; j++)
                {
                    //bomb meledak
                    if (arr_unit[i].bomb[j].meledak == true)
                    {
                        if (arr_unit[i].bomb[j].ctrbomb < 3)
                        {
                            if (arr_unit[i].bomb[j].ctrbomb == 0)
                            {
                                arr_unit[i].bomb[j].label_unit.Image = new Bitmap("bomb2.png");
                            }
                            arr_unit[i].bomb[j].ctrbomb++;
                        }
                        else if (arr_unit[i].bomb[j].ctrbomb == 3)
                        {
                            arr_unit[i].bomb[j].meledak = false;
                            arr_unit[i].bomb[j].ctrbomb = 0;
                            arr_unit[i].bomb[j].label_unit.Image = null;
                            arr_unit[i].bomb[j].ledakan = true;
                        }
                    }
                }
            }
        }
        private void timerledak_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Count(); i++)
            {
                for (int j = 0; j < arr_unit[i].bomb.Count; j++)
                {
                    //ledakan
                    if (arr_unit[i].bomb[j].ledakan == true)
                    {
                        if (arr_unit[i].bomb[j].ctrledak < 4)
                        {
                            if (arr_unit[i].bomb[j].ctrledak == 0)
                            {
                                arr_unit[i].bomb[j].label_ledakan.Clear();
                                Label z = new Label();
                                z.Size = new Size(50, 50);
                                arr_unit[i].bomb[j].label_ledakan.Add(z);
                                for (int k = 0; k < arr_unit[i].bomb[j].isi_bomb.vertical; k++)
                                {
                                    Label x = new Label();
                                    x.Size = new Size(50, 50);
                                    arr_unit[i].bomb[j].label_ledakan.Add(x);

                                    Label y = new Label();
                                    y.Size = new Size(50, 50);
                                    arr_unit[i].bomb[j].label_ledakan.Add(y);
                                }
                                for (int k = 0; k < arr_unit[i].bomb[j].isi_bomb.horizontal; k++)
                                {
                                    Label x = new Label();
                                    x.Size = new Size(50, 50);
                                    arr_unit[i].bomb[j].label_ledakan.Add(x);

                                    Label y = new Label();
                                    y.Size = new Size(50, 50);
                                    arr_unit[i].bomb[j].label_ledakan.Add(y);
                                }
                                int ctrhorizontal1 = 0;
                                int ctrhorizontal2 = 0;
                                int ctrvertical1 = 0;
                                int ctrvertical2 = 0;
                                int horizontal1 = arr_unit[i].bomb[j].label_unit.Location.X - 50;
                                int horizontal2 = arr_unit[i].bomb[j].label_unit.Location.X + 50;
                                int vertical1 = arr_unit[i].bomb[j].label_unit.Location.Y - 50;
                                int vertical2 = arr_unit[i].bomb[j].label_unit.Location.Y + 50;
                                foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                                {
                                    if (ctrhorizontal1 < arr_unit[i].bomb[j].isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal1, arr_unit[i].bomb[j].label_unit.Location.Y);
                                        ctrhorizontal1++;
                                        horizontal1 -= 50;
                                    }
                                    else if (ctrhorizontal2 < arr_unit[i].bomb[j].isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal2, arr_unit[i].bomb[j].label_unit.Location.Y);
                                        ctrhorizontal2++;
                                        horizontal2 += 50;
                                    }
                                    else if (ctrvertical1 < arr_unit[i].bomb[j].isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].bomb[j].label_unit.Location.X, vertical1);
                                        ctrvertical1++;
                                        vertical1 -= 50;
                                    }
                                    else if (ctrvertical2 < arr_unit[i].bomb[j].isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].bomb[j].label_unit.Location.X, vertical2);
                                        ctrvertical2++;
                                        vertical2 += 50;
                                    }
                                    else
                                    {
                                        x.Location = new Point(arr_unit[i].bomb[j].label_unit.Location.X, arr_unit[i].bomb[j].label_unit.Location.Y);
                                    }
                                    x.Image = new Bitmap("explosion1.png");
                                    x.BackColor = Color.Transparent;
                                    x.FlatStyle = FlatStyle.Flat;
                                    x.Parent = lantai;
                                    x.BringToFront();
                                }
                            }
                            else if (arr_unit[i].bomb[j].ctrledak == 1)
                            {
                                int ctrhorizontal1 = 0;
                                int ctrhorizontal2 = 0;
                                int ctrvertical1 = 0;
                                int ctrvertical2 = 0;
                                int horizontal1 = arr_unit[i].bomb[j].label_unit.Location.X - 50;
                                int horizontal2 = arr_unit[i].bomb[j].label_unit.Location.X + 50;
                                int vertical1 = arr_unit[i].bomb[j].label_unit.Location.Y - 50;
                                int vertical2 = arr_unit[i].bomb[j].label_unit.Location.Y + 50;
                                foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                                {
                                    x.Image = new Bitmap("explosion1.png");
                                    if (ctrhorizontal1 < arr_unit[i].bomb[j].isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal1, arr_unit[i].bomb[j].label_unit.Location.Y);
                                        ctrhorizontal1++;
                                        horizontal1 -= 50;
                                    }
                                    else if (ctrhorizontal2 < arr_unit[i].bomb[j].isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal2, arr_unit[i].bomb[j].label_unit.Location.Y);
                                        ctrhorizontal2++;
                                        horizontal2 += 50;
                                    }
                                    else if (ctrvertical1 < arr_unit[i].bomb[j].isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].bomb[j].label_unit.Location.X, vertical1);
                                        ctrvertical1++;
                                        vertical1 -= 50;
                                    }
                                    else if (ctrvertical2 < arr_unit[i].bomb[j].isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].bomb[j].label_unit.Location.X, vertical2);
                                        ctrvertical2++;
                                        vertical2 += 50;
                                    }
                                    else
                                    {
                                        x.Location = new Point(arr_unit[i].bomb[j].label_unit.Location.X, arr_unit[i].bomb[j].label_unit.Location.Y);
                                    }
                                    x.BringToFront();
                                }
                                //hancur
                                System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"suara.wav");
                                player.Play();
                                List<obstacle> list_remove = new List<obstacle>();
                                foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                                {
                                    foreach (obstacle y in list_isi_map)
                                    {
                                        if (x.Location.X == y.label_unit.Location.X && x.Location.Y == y.label_unit.Location.Y && y.jenis == "papan")
                                        {
                                            list_remove.Add(y);
                                        }
                                    }
                                }
                                foreach (obstacle x in list_remove)
                                {
                                    x.label_unit.Image = null;
                                    x.label_unit.SendToBack();
                                    list_isi_map.Remove(x);
                                }
                                foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                                {
                                    for (int k = 0; k < arr_unit.Length; k++)
                                    {
                                        if (x.Location.X == arr_unit[k].label_unit.Location.X && x.Location.Y == arr_unit[k].label_unit.Location.Y)
                                        {
                                            arr_unit[k].label_unit.Image = null;
                                            arr_unit[k].mana.SendToBack();
                                        }
                                    }
                                }
                            }
                            else if (arr_unit[i].bomb[j].ctrledak == 2)
                            {
                                foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                                {
                                    x.Image = new Bitmap("explosion2.png");
                                    x.BringToFront();
                                }
                            }
                            else if (arr_unit[i].bomb[j].ctrledak == 3)
                            {
                                foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                                {
                                    x.Image = new Bitmap("explosion3.png");
                                    x.BringToFront();
                                }
                            }
                            arr_unit[i].bomb[j].ctrledak++;
                        }
                        else if(arr_unit[i].bomb[j].ctrledak == 4)
                        {
                            foreach (Label x in arr_unit[i].bomb[j].label_ledakan)
                            {
                                x.Image = null;
                                x.SendToBack();
                            }
                            arr_unit[i].bomb[j].ctrledak = 0;
                            arr_unit[i].bomb[j].ledakan = false;
                            arr_unit[i].label_unit.BringToFront();
                        }
                    }
                }
            }
        }
        private void eagle_eye_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Length; i++)
            {
                if (arr_unit[i].use_skill == true && arr_unit[i].bag.skill =="eagle eye")
                {
                    foreach (obstacle x in list_isi_map)
                    {
                        if (x.jenis == "mine")
                        {
                            x.label_unit.BringToFront();
                        }
                    }
                    if (arr_unit[i].ctrtimer == arr_unit[i].bag.timeEE)
                    {
                        foreach (obstacle x in list_isi_map)
                        {
                            if (x.jenis == "papan")
                            {
                                x.label_unit.BringToFront();
                            }
                        }
                        arr_unit[i].use_skill = false;
                        arr_unit[i].ctrtimer = 0;
                    }
                    arr_unit[i].ctrtimer++;
                }
            }
        }

        private void mine_animation_Tick(object sender, EventArgs e)
        {
            foreach (obstacle x in list_isi_map)
            {
                if (x.jenis=="mine" && x.stepped==false)
                {
                    if (animasi_mine%2==0)
                    {
                        x.label_unit.Image = new Bitmap("potato00.png");
                    }
                    else
                    {
                        x.label_unit.Image = new Bitmap("potato01.png");
                    }
                }
            }
            for (int i = 0; i < arr_unit.Length; i++)
            {
                if (arr_unit[i].mine.plantinmap == true)
                {
                    if (animasi_mine % 2 == 0)
                    {
                        arr_unit[i].mine.label_unit.Image = new Bitmap("potato10.png");
                    }
                    else
                    {
                        arr_unit[i].mine.label_unit.Image = new Bitmap("potato11.png");
                    }
                }
                else
                {
                    arr_unit[i].mine.label_unit.Image = null;
                }
            }
            if (animasi_mine==1000)
            {
                animasi_mine = 0;
            }
            animasi_mine++;
        }

        private void cek_win_Tick(object sender, EventArgs e)
        {
            int ctr = 0;
            int player = -1;
            for (int i = 0; i < arr_unit.Length; i++)
            {
                if (arr_unit[i].label_unit.Image==null)
                {
                    ctr++;
                }
                else
                {
                    player = i+1;
                }
            }
            if (ctr==3)
            {
                cek_win.Stop();
                MessageBox.Show("PLAYER "+player+" WIN!!!");
                arr_unit[player-1].bag.uang+=100;
                save_player_stat();
                ((Form1)MdiParent).change_to_menu_game();
            }
            if (ctr==4)
            {
                cek_win.Stop();
                MessageBox.Show("DRAW!!!");
                ((Form1)MdiParent).change_to_menu_game();
            }
        }

        private void step_mine_Tick(object sender, EventArgs e)
        {
            //player hancur
            foreach (obstacle x in list_isi_map)
            {
                for (int i = 0; i < arr_unit.Length; i++)
                {
                    if (x.label_unit.Location.X == arr_unit[i].label_unit.Location.X && x.label_unit.Location.Y == arr_unit[i].label_unit.Location.Y && x.jenis == "mine")
                    {
                        arr_unit[i].label_unit.Image = null;
                        arr_unit[i].mana.SendToBack();
                        arr_unit[i].label_unit.SendToBack();
                        x.label_unit.BringToFront();
                        x.stepped = true;
                    }
                }
            }
        }
        private void mine_meledak_Tick(object sender, EventArgs e)
        {
            List<obstacle> list_remove = new List<obstacle>();
            foreach (obstacle x in list_isi_map)
            {
                if (x.stepped == true)
                {
                    if (x.jenis == "mine" && x.ctr == 0)
                    {
                        x.label_unit.Image = new Bitmap("explosion1.png");
                        x.label_unit.BringToFront();
                        x.ctr++;
                    }
                    else if (x.jenis == "mine" && x.ctr == 1)
                    {
                        x.label_unit.Image = new Bitmap("explosion2.png");
                        x.label_unit.BringToFront();
                        x.ctr++;
                    }
                    else if (x.jenis == "mine" && x.ctr == 2)
                    {
                        x.label_unit.Image = new Bitmap("explosion3.png");
                        x.label_unit.BringToFront();
                        x.ctr++;
                    }
                    else
                    {
                        x.label_unit.SendToBack();
                        list_remove.Add(x);
                    }
                }
            }
            foreach (obstacle x in list_remove)
            {
                x.label_unit.Image = null;
                x.label_unit.SendToBack();
                list_isi_map.Remove(x);
            }
        }

        private void refresh_mana_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Length; i++)
            {
                if (arr_unit[i].mana.Value+3<=100)
                {
                    arr_unit[i].mana.Value += 3;
                }
                else
                {
                    arr_unit[i].mana.Value = 100;
                }
            }
        }

        private void speed_up_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Length; i++)
            {
                if (arr_unit[i].use_skill == true && arr_unit[i].bag.skill == "speed up")
                {
                    if (arr_unit[i].ctrtimer == 0)
                    {
                        arr_unit[i].bag.Movement_Speed -= arr_unit[i].bag.speed_up;
                    }
                    else if (arr_unit[i].ctrtimer == arr_unit[i].bag.time_speed_up)
                    {
                        arr_unit[i].bag.Movement_Speed += arr_unit[i].bag.speed_up;
                        arr_unit[i].use_skill = false;
                        arr_unit[i].ctrtimer = 0;
                    }
                    arr_unit[i].ctrtimer++;
                }
            }
        }

        private void timer_mine_player_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Count(); i++)
            {
                //plant mine
                if (arr_unit[i].planting_bomb == true)
                {
                    int index = -1;
                    if (arr_unit[i].mine.ctrbomb < 3 && arr_unit[i].mine.tanam == true && arr_unit[i].bag.skill =="mine_bomb")
                    {
                        arr_unit[i].label_unit.Image = arr_unit[i].image_bomb[arr_unit[i].mine.ctrbomb];
                        if (arr_unit[i].mine.ctrbomb == 0)
                        {
                            arr_unit[i].mine.label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y + 50);
                            arr_unit[i].mine.label_unit.Image = new Bitmap("potato10.png");
                            this.Controls.Add(arr_unit[i].mine.label_unit);
                            arr_unit[i].mine.label_unit.BackColor = Color.Transparent;
                            arr_unit[i].mine.label_unit.FlatStyle = FlatStyle.Flat;
                            arr_unit[i].mine.label_unit.Parent = lantai;
                            arr_unit[i].mine.label_unit.BringToFront();
                        }
                        else if (arr_unit[i].mine.ctrbomb == 1)
                        {
                            arr_unit[i].mine.label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y - 50);
                        }
                        else
                        {
                            arr_unit[i].mine.label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y);
                            arr_unit[i].label_unit.BringToFront();
                        }
                        arr_unit[i].mine.ctrbomb++;
                    }
                    else if (arr_unit[i].mine.ctrbomb == 3 && arr_unit[i].mine.tanam == true)
                    {
                        arr_unit[i].planting_bomb = false;
                        arr_unit[i].mine.tanam = false;
                        arr_unit[i].use_skill = false;
                        arr_unit[i].mine.ctrbomb = 0;
                        arr_unit[i].mine.plantinmap = true;
                        arr_unit[i].gerak = false;
                    }
                }
            }
        }

        private void timer_ledak_bomb_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < arr_unit.Count(); i++)
            {
                    //ledakan
                    if (arr_unit[i].mine.ledakan == true)
                    {
                        if (arr_unit[i].mine.ctrledak < 4)
                        {
                            if (arr_unit[i].mine.ctrledak == 0)
                            {
                                arr_unit[i].mine.label_ledakan.Clear();
                                Label z = new Label();
                                z.Size = new Size(50, 50);
                                arr_unit[i].mine.label_ledakan.Add(z);
                                for (int k = 0; k < arr_unit[i].mine.isi_bomb.vertical; k++)
                                {
                                    Label x = new Label();
                                    x.Size = new Size(50, 50);
                                    arr_unit[i].mine.label_ledakan.Add(x);

                                    Label y = new Label();
                                    y.Size = new Size(50, 50);
                                    arr_unit[i].mine.label_ledakan.Add(y);
                                }
                                for (int k = 0; k < arr_unit[i].mine.isi_bomb.horizontal; k++)
                                {
                                    Label x = new Label();
                                    x.Size = new Size(50, 50);
                                    arr_unit[i].mine.label_ledakan.Add(x);

                                    Label y = new Label();
                                    y.Size = new Size(50, 50);
                                    arr_unit[i].mine.label_ledakan.Add(y);
                                }
                                int ctrhorizontal1 = 0;
                                int ctrhorizontal2 = 0;
                                int ctrvertical1 = 0;
                                int ctrvertical2 = 0;
                                int horizontal1 = arr_unit[i].mine.label_unit.Location.X - 50;
                                int horizontal2 = arr_unit[i].mine.label_unit.Location.X + 50;
                                int vertical1 = arr_unit[i].mine.label_unit.Location.Y - 50;
                                int vertical2 = arr_unit[i].mine.label_unit.Location.Y + 50;
                                foreach (Label x in arr_unit[i].mine.label_ledakan)
                                {
                                    if (ctrhorizontal1 < arr_unit[i].mine.isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal1, arr_unit[i].mine.label_unit.Location.Y);
                                        ctrhorizontal1++;
                                        horizontal1 -= 50;
                                    }
                                    else if (ctrhorizontal2 < arr_unit[i].mine.isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal2, arr_unit[i].mine.label_unit.Location.Y);
                                        ctrhorizontal2++;
                                        horizontal2 += 50;
                                    }
                                    else if (ctrvertical1 < arr_unit[i].mine.isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].mine.label_unit.Location.X, vertical1);
                                        ctrvertical1++;
                                        vertical1 -= 50;
                                    }
                                    else if (ctrvertical2 < arr_unit[i].mine.isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].mine.label_unit.Location.X, vertical2);
                                        ctrvertical2++;
                                        vertical2 += 50;
                                    }
                                    else
                                    {
                                        x.Location = new Point(arr_unit[i].mine.label_unit.Location.X, arr_unit[i].mine.label_unit.Location.Y);
                                    }
                                    x.Image = new Bitmap("explosion1.png");
                                    x.BackColor = Color.Transparent;
                                    x.FlatStyle = FlatStyle.Flat;
                                    x.Parent = lantai;
                                    x.BringToFront();
                                }
                                for (int j = 0; j < arr_unit.Length; j++)
                                {
                                    if (arr_unit[j].label_unit.Location.X == arr_unit[i].mine.label_unit.Location.X && arr_unit[j].label_unit.Location.Y == arr_unit[i].mine.label_unit.Location.Y && i != j)
                                    {
                                        arr_unit[j].label_unit.Image = null;
                                        arr_unit[j].mana.SendToBack();
                                        arr_unit[j].label_unit.SendToBack();
                                    }
                                }
                            }
                            else if (arr_unit[i].mine.ctrledak == 1)
                            {
                                int ctrhorizontal1 = 0;
                                int ctrhorizontal2 = 0;
                                int ctrvertical1 = 0;
                                int ctrvertical2 = 0;
                                int horizontal1 = arr_unit[i].mine.label_unit.Location.X - 50;
                                int horizontal2 = arr_unit[i].mine.label_unit.Location.X + 50;
                                int vertical1 = arr_unit[i].mine.label_unit.Location.Y - 50;
                                int vertical2 = arr_unit[i].mine.label_unit.Location.Y + 50;
                                foreach (Label x in arr_unit[i].mine.label_ledakan)
                                {
                                    x.Image = new Bitmap("explosion1.png");
                                    if (ctrhorizontal1 < arr_unit[i].mine.isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal1, arr_unit[i].mine.label_unit.Location.Y);
                                        ctrhorizontal1++;
                                        horizontal1 -= 50;
                                    }
                                    else if (ctrhorizontal2 < arr_unit[i].mine.isi_bomb.horizontal)
                                    {
                                        x.Location = new Point(horizontal2, arr_unit[i].mine.label_unit.Location.Y);
                                        ctrhorizontal2++;
                                        horizontal2 += 50;
                                    }
                                    else if (ctrvertical1 < arr_unit[i].mine.isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].mine.label_unit.Location.X, vertical1);
                                        ctrvertical1++;
                                        vertical1 -= 50;
                                    }
                                    else if (ctrvertical2 < arr_unit[i].mine.isi_bomb.vertical)
                                    {
                                        x.Location = new Point(arr_unit[i].mine.label_unit.Location.X, vertical2);
                                        ctrvertical2++;
                                        vertical2 += 50;
                                    }
                                    else
                                    {
                                        x.Location = new Point(arr_unit[i].mine.label_unit.Location.X, arr_unit[i].mine.label_unit.Location.Y);
                                    }
                                    x.BringToFront();
                                }
                                //hancur
                                List<obstacle> list_remove = new List<obstacle>();
                                foreach (Label x in arr_unit[i].mine.label_ledakan)
                                {
                                    foreach (obstacle y in list_isi_map)
                                    {
                                        if (x.Location.X == y.label_unit.Location.X && x.Location.Y == y.label_unit.Location.Y && y.jenis == "papan")
                                        {
                                            list_remove.Add(y);
                                        }
                                    }
                                }
                                foreach (obstacle x in list_remove)
                                {
                                    x.label_unit.Image = null;
                                    x.label_unit.SendToBack();
                                    list_isi_map.Remove(x);
                                }
                            }
                            else if (arr_unit[i].mine.ctrledak == 2)
                            {
                                foreach (Label x in arr_unit[i].mine.label_ledakan)
                                {
                                    x.Image = new Bitmap("explosion2.png");
                                    x.BringToFront();
                                }
                            }
                            else if (arr_unit[i].mine.ctrledak == 3)
                            {
                                foreach (Label x in arr_unit[i].mine.label_ledakan)
                                {
                                    x.Image = new Bitmap("explosion3.png");
                                    x.BringToFront();
                                }
                            }
                            arr_unit[i].mine.ctrledak++;
                        }
                        else if (arr_unit[i].mine.ctrledak == 4)
                        {
                            foreach (Label x in arr_unit[i].mine.label_ledakan)
                            {
                                x.Image = null;
                                x.SendToBack();
                            }
                            arr_unit[i].mine.ctrledak = 0;
                            arr_unit[i].mine.ledakan = false;
                            arr_unit[i].label_unit.BringToFront();
                            arr_unit[i].mine.plantinmap = false;
                        }
                    }
            }
        }

        private void step_mine_player_Tick(object sender, EventArgs e)
        {
            //cek player injak bomb
            for (int i = 0; i < arr_unit.Length; i++)
            {
                for (int j = 0; j < arr_unit.Length; j++)
                {
                    if (arr_unit[j].label_unit.Location.X == arr_unit[i].mine.label_unit.Location.X && arr_unit[j].label_unit.Location.Y == arr_unit[i].mine.label_unit.Location.Y && i != j && arr_unit[j].label_unit.Image != null)
                    {
                        arr_unit[i].mine.ledakan = true;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int gerak = 0;
            foreach (obstacle x in list_isi_map)
            {
                if (arr_unit[0].label_unit.Location.X == x.label_unit.Location.X && arr_unit[0].label_unit.Location.Y == x.label_unit.Location.Y && x.jenis == "choosen")
                {
                    x.jenis = "lantai";
                }
            }
            foreach (obstacle x in list_isi_map)
            {
                //kanan
                if (arr_unit[0].label_unit.Location.X + 50 == x.label_unit.Location.X && arr_unit[0].label_unit.Location.Y == x.label_unit.Location.Y && x.jenis == "choosen" && gerak == 0)
                {
                    kanan(0);
                    x.jenis = "lantai";
                    gerak = 1;
                }
                //kiri
                else if (arr_unit[0].label_unit.Location.X - 50 == x.label_unit.Location.X && arr_unit[0].label_unit.Location.Y == x.label_unit.Location.Y && x.jenis == "choosen" && gerak == 0)
                {
                    kiri(0);
                    x.jenis = "lantai";
                    gerak = 1;
                }
                //atas
                else if (arr_unit[0].label_unit.Location.X == x.label_unit.Location.X && arr_unit[0].label_unit.Location.Y - 50 == x.label_unit.Location.Y && x.jenis == "choosen" && gerak == 0)
                {
                    atas(0);
                    x.jenis = "lantai";
                    gerak = 1;
                }
                //bawah
                else if (arr_unit[0].label_unit.Location.X == x.label_unit.Location.X && arr_unit[0].label_unit.Location.Y + 50 == x.label_unit.Location.Y && x.jenis == "choosen" && gerak == 0)
                {
                    bawah(0);
                    x.jenis = "lantai";
                    gerak = 1;
                }
            }
        }
    }
}
