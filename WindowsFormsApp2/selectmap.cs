﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class selectmap : Form
    {
        public int map = 0;
        public List<isi_map> list_isi_map = new List<isi_map>();
        public List<string> file_map = new List<string>();
        string[,] temp = new string[0, 0];
        public selectmap()
        {
            InitializeComponent();
        }

        private void selectmap_Load(object sender, EventArgs e)
        {
            this.Focus();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //button choose
            this.btnchoose.BackgroundImage = new Bitmap("buttonchoose.png");
            this.btnchoose.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnchoose.BackColor = Color.Transparent;
            this.btnchoose.FlatStyle = FlatStyle.Flat;
            //button arrow left
            this.arrow_left.BackgroundImage = new Bitmap("arrow1.PNG");
            this.arrow_left.BackgroundImageLayout = ImageLayout.Stretch;
            this.arrow_left.BackColor = Color.Transparent;
            this.arrow_left.FlatStyle = FlatStyle.Flat;
            //button arrow right
            this.arrow_right.BackgroundImage = new Bitmap("arrow2.PNG");
            this.arrow_right.BackgroundImageLayout = ImageLayout.Stretch;
            this.arrow_right.BackColor = Color.Transparent;
            this.arrow_right.FlatStyle = FlatStyle.Flat;
            //isi map
            file_map.Add("map1.txt");
            file_map.Add("map2.txt");
            file_map.Add("map3.txt");
            Remake_Map();
        }

        private void btnchoose_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_map();
        }

        private void gambar_map_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (map <3)
            {
                foreach (isi_map x in list_isi_map)
                {
                    if (x.jenis == "lantai")
                    {
                        if (map == 0) g.DrawImage(new Bitmap("forest_tile.png"), x.x, x.y, 30, 30);
                        else if (map == 1) g.DrawImage(new Bitmap("sea_tile.png"), x.x, x.y, 30, 30);
                        else if (map == 2) g.DrawImage(new Bitmap("dragon_tile.png"), x.x, x.y, 30, 30);
                    }
                    else if (x.jenis == "tembok")
                    {
                        if (map == 0) g.DrawImage(new Bitmap("forest_rock.png"), x.x, x.y, 30, 30);
                        else if (map == 1) g.DrawImage(new Bitmap("sea_rock.png"), x.x, x.y, 30, 30);
                        else if (map == 2) g.DrawImage(new Bitmap("dragon_rock.png"), x.x, x.y, 30, 30);
                    }
                    else if (x.jenis == "papan")
                    {
                        if (map == 0) g.DrawImage(new Bitmap("forest_tree.png"), x.x, x.y, 30, 30);
                        else if (map == 1) g.DrawImage(new Bitmap("sea_box.png"), x.x, x.y, 30, 30);
                        else if (map == 2) g.DrawImage(new Bitmap("dragon_wall.png"), x.x, x.y, 30, 30);
                    }
                }
            }
            else
            {
                for (int i = 0; i < 15; i++)
                {
                    for (int j = 0; j < 20; j++)
                    {
                        if (i == 0 && j == 0)
                        {
                            g.DrawImage(new Bitmap("pojokKiri.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (i == 0 && j == 19)
                        {
                            g.DrawImage(new Bitmap("pojokKanan.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (i == 14 && j == 0)
                        {
                            g.DrawImage(new Bitmap("pojokBawahKiri.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (i == 14 && j == 19)
                        {
                            g.DrawImage(new Bitmap("pojokBawahKanan.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (i == 0)
                        {
                            g.DrawImage(new Bitmap("atas.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (i == 14)
                        {
                            g.DrawImage(new Bitmap("bawah.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (j == 19)
                        {
                            g.DrawImage(new Bitmap("kanan.png"), j * 30, i * 30, 30, 30);
                        }
                        else if (j == 0)
                        {
                            g.DrawImage(new Bitmap("kiri.png"), j * 30, i * 30, 30, 30);
                        }
                        else
                        {
                            g.DrawImage(new Bitmap("tengah.png"), j * 30, i * 30, 30, 30);
                        }
                    }
                }
                if (temp != null)
                {
                    for (int i = 0; i < 15; i++)
                    {
                        for (int j = 0; j < 20; j++)
                        {
                            if (temp[i, j] == "1")
                            {
                                g.DrawImage(new Bitmap("wall1.png"), j * 30, i * 30, 30, 30);
                            }
                            else if (temp[i, j] == "0")
                            {
                                g.DrawImage(new Bitmap("wall0.png"), j * 30, i * 30, 30, 30);
                            }
                        }
                    }
                }
            }
        }
        public void Remake_Map()
        {
            if (map<3)
            {
                list_isi_map.Clear();
                int i = 0;
                int j = 0;
                string[] lines = lines = System.IO.File.ReadAllLines(@"" + file_map[map]);
                foreach (string line in lines)
                {
                    string[] word = line.Split('-');
                    foreach (string karakter in word)
                    {
                        isi_map x = new isi_map(j * 30, i * 30, "lantai");
                        list_isi_map.Add(x);
                        if (karakter == "0")
                        {
                            isi_map y = new isi_map(j * 30, i * 30, "tembok");
                            list_isi_map.Add(y);
                        }
                        else if (karakter == "2")
                        {
                            isi_map y = new isi_map(j * 30, i * 30, "papan");
                            list_isi_map.Add(y);
                        }
                        j++;
                    }
                    j = 0;
                    i++;
                }
            }
            else
            {
                this.temp = ((Form1)MdiParent).arena[0];
            }
        }

        private void arrow_left_Click(object sender, EventArgs e)
        {
            if (map-1>=0)
            {
                map--;
                Remake_Map();
                gambar_map.Invalidate();
            }
        }

        private void arrow_right_Click(object sender, EventArgs e)
        {
            if (map+1<=3)
            {
                map++;
                Remake_Map();
                gambar_map.Invalidate();
            }
        }
    }
}
