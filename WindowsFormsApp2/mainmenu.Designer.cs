﻿namespace WindowsFormsApp2
{
    partial class mainmenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playbutton = new System.Windows.Forms.Button();
            this.loadbutton = new System.Windows.Forms.Button();
            this.exitbutton = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // playbutton
            // 
            this.playbutton.Location = new System.Drawing.Point(747, 374);
            this.playbutton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.playbutton.Name = "playbutton";
            this.playbutton.Size = new System.Drawing.Size(364, 76);
            this.playbutton.TabIndex = 0;
            this.playbutton.UseVisualStyleBackColor = true;
            this.playbutton.Click += new System.EventHandler(this.playbutton_Click);
            this.playbutton.MouseLeave += new System.EventHandler(this.playbutton_MouseLeave);
            this.playbutton.MouseHover += new System.EventHandler(this.playbutton_MouseHover);
            // 
            // loadbutton
            // 
            this.loadbutton.Location = new System.Drawing.Point(747, 476);
            this.loadbutton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.loadbutton.Name = "loadbutton";
            this.loadbutton.Size = new System.Drawing.Size(364, 76);
            this.loadbutton.TabIndex = 1;
            this.loadbutton.UseVisualStyleBackColor = true;
            this.loadbutton.Click += new System.EventHandler(this.loadbutton_Click);
            this.loadbutton.MouseLeave += new System.EventHandler(this.loadbutton_MouseLeave);
            this.loadbutton.MouseHover += new System.EventHandler(this.loadbutton_MouseHover);
            // 
            // exitbutton
            // 
            this.exitbutton.Location = new System.Drawing.Point(747, 576);
            this.exitbutton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitbutton.Name = "exitbutton";
            this.exitbutton.Size = new System.Drawing.Size(364, 76);
            this.exitbutton.TabIndex = 2;
            this.exitbutton.UseVisualStyleBackColor = true;
            this.exitbutton.Click += new System.EventHandler(this.exitbutton_Click);
            this.exitbutton.MouseLeave += new System.EventHandler(this.exitbutton_MouseLeave);
            this.exitbutton.MouseHover += new System.EventHandler(this.exitbutton_MouseHover);
            // 
            // title
            // 
            this.title.Location = new System.Drawing.Point(501, 37);
            this.title.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(836, 234);
            this.title.TabIndex = 3;
            this.title.UseVisualStyleBackColor = true;
            // 
            // mainmenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 750);
            this.Controls.Add(this.title);
            this.Controls.Add(this.exitbutton);
            this.Controls.Add(this.loadbutton);
            this.Controls.Add(this.playbutton);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "mainmenu";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button playbutton;
        private System.Windows.Forms.Button loadbutton;
        private System.Windows.Forms.Button exitbutton;
        private System.Windows.Forms.Button title;
    }
}