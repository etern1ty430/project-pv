﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WindowsFormsApp2
{
    public class unit
    {
        public int x, y, arah, idx;
        public Image gambar;
        public unit(int x, int y, Image gambar, int idx)
        {
            this.x = x;
            this.y = y;
            this.gambar = gambar;
            arah = 3;
            this.idx = idx;
        }
        public unit(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public unit()
        {
             
        }
    }
}
