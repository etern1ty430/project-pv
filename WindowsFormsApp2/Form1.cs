﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using RawInput_dll;
using System.Media;
namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public IntPtr[] keyboards = new IntPtr[4];
        private bool[] pReady = new bool[4];
        public SoundPlayer player = new System.Media.SoundPlayer(@"soundtrackgame.wav");
        RawInput rawInput;
        //DECLARE FORM
        mainmenu main_menu_game = new mainmenu();
        loadingscreen loading_screen_game = new loadingscreen();
        public selectplayer menu_select_player = new selectplayer();
        public select_player_load menu_select_player_load = new select_player_load();
        menugame menu_main_game = new menugame();
        shop_game menu_shop_game;
        public map Map;
        public selectmap menu_select_map = new selectmap();
        public List<string[,]> arena = new List<string[,]>();
        public int save_or_load=-1;
        public int first_time_play = 0;
        public Form1()
        {
            InitializeComponent();
            load_map();
            rawInput = new RawInput(Handle, true);
            rawInput.KeyPressed += RawInput_KeyPressed;
        }
        private void RawInput_KeyPressed(object sender, RawInputEventArg e)
        {
            for (int i = 0; i < 3; i++)
            {
                if (keyboards[i] == IntPtr.Zero)
                {
                    keyboards[i] = e.KeyPressEvent.DeviceHandle;
                    break;
                }
                else if (keyboards[i] == e.KeyPressEvent.DeviceHandle)
                    break;
            }
            //keyboard 1
            if (e.KeyPressEvent.DeviceHandle == keyboards[0] && Map != null)
            {
                //player 1
                if (e.KeyPressEvent.VKeyName == "W")
                {
                    Map.atas(0);
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    Map.bawah(0);
                }
                else if (e.KeyPressEvent.VKeyName == "A")
                {
                    Map.kiri(0);
                }
                else if (e.KeyPressEvent.VKeyName == "D")
                {
                    Map.kanan(0);
                }
                else if (e.KeyPressEvent.VKeyName == "SPACE")
                {
                    Map.Plant_Bomb(0);
                }
                else if (e.KeyPressEvent.VKeyName == "X")
                {
                    Map.skill(0);
                }
                //player 2
                if (e.KeyPressEvent.VKeyName == "I")
                {
                    Map.atas(1);
                }
                else if (e.KeyPressEvent.VKeyName == "K")
                {
                    Map.bawah(1);
                }
                else if (e.KeyPressEvent.VKeyName == "J")
                {
                    Map.kiri(1);
                }
                else if (e.KeyPressEvent.VKeyName == "L")
                {
                    Map.kanan(1);
                }
                else if (e.KeyPressEvent.VKeyName == "M")
                {
                    Map.Plant_Bomb(1);
                }
                else if (e.KeyPressEvent.VKeyName == "N")
                {
                    Map.skill(1);
                }
            }
            else if (e.KeyPressEvent.DeviceHandle == keyboards[1] && Map != null)
            {
                //player 3
                if (e.KeyPressEvent.VKeyName == "W")
                {
                    Map.atas(2);
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    Map.bawah(2);
                }
                else if (e.KeyPressEvent.VKeyName == "A")
                {
                    Map.kiri(2);
                }
                else if (e.KeyPressEvent.VKeyName == "D")
                {
                    Map.kanan(2);
                }
                else if (e.KeyPressEvent.VKeyName == "SPACE")
                {
                    Map.Plant_Bomb(2);
                }
                else if (e.KeyPressEvent.VKeyName == "X")
                {
                    Map.skill(2);
                }
                //player 4
                if (e.KeyPressEvent.VKeyName == "I")
                {
                    Map.atas(3);
                }
                else if (e.KeyPressEvent.VKeyName == "K")
                {
                    Map.bawah(3);
                }
                else if (e.KeyPressEvent.VKeyName == "J")
                {
                    Map.kiri(3);
                }
                else if (e.KeyPressEvent.VKeyName == "L")
                {
                    Map.kanan(3);
                }
                else if (e.KeyPressEvent.VKeyName == "M")
                {
                    Map.Plant_Bomb(3);
                }
                else if (e.KeyPressEvent.VKeyName == "N")
                {
                    Map.skill(3);
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            player.Play();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            //setting form
            this.IsMdiContainer = true;
            //loading screen
            loading_screen_game.MdiParent = this;
            loading_screen_game.Text = "loading screen";
            loading_screen_game.Show();
            //main menu
            main_menu_game.MdiParent = this;
            main_menu_game.Text = "main menu";
            main_menu_game.Show();
            main_menu_game.Location = new Point(0, 0);
            main_menu_game.Visible=false;
            //menu select player
            menu_select_player.MdiParent = this;
            menu_select_player.Show();
            menu_select_player.Location = new Point(0, 0);
            menu_select_player.Visible = false;
            //menu select player load
            menu_select_player_load.MdiParent = this;
            menu_select_player_load.Show();
            menu_select_player_load.Location = new Point(0, 0);
            menu_select_player_load.Visible = false;
            //menu game
            menu_main_game.MdiParent = this;
            menu_main_game.Show();
            menu_main_game.Location = new Point(0, 0);
            menu_main_game.Visible = false;
            //menu select player
            menu_select_map.MdiParent = this;
            menu_select_map.Show();
            menu_select_map.Location = new Point(0, 0);
            menu_select_map.Visible = false;
        }
        public void change_to_shop()
        {
            if (first_time_play==1)
            {
                menu_main_game.Visible = false;
                menu_shop_game = new shop_game();
                menu_shop_game.MdiParent = this;
                menu_shop_game.Show();
                menu_shop_game.Visible = false;
                menu_shop_game.Visible = true;
                menu_shop_game.Location = new Point(0, 0);
                menu_shop_game.BackColor = Color.Black;
                menu_shop_game.WindowState = FormWindowState.Maximized;
                menu_shop_game.ControlBox = false;
                menu_shop_game.MaximizeBox = false;
                menu_shop_game.MinimizeBox = false;
                menu_shop_game.ShowIcon = false;
                menu_shop_game.Text = "";
                menu_shop_game.Dock = DockStyle.Fill;
                menu_shop_game.panel1.BackgroundImageLayout = ImageLayout.Stretch;
                menu_shop_game.BackgroundImage = new Bitmap("bgKuning.png");
                save_or_load = 1;
            }
            else
            {
                MessageBox.Show("CANT GO TO SHOP, UNTIL YOU DO YOUR FIRST BATTLE!");
            }
        }
        public void change_to_mainmenu()
        {
            loading_screen_game.Visible = false;
            menu_select_player_load.Visible = false;
            menu_select_player.Visible = false;
            menu_main_game.Visible = false;
            main_menu_game.Visible = true;
            main_menu_game.Location = new Point(0,0);
            main_menu_game.BackColor = Color.Black;
            main_menu_game.WindowState = FormWindowState.Maximized;
            main_menu_game.ControlBox = false;
            main_menu_game.MaximizeBox = false;
            main_menu_game.MinimizeBox = false;
            main_menu_game.ShowIcon = false;
            main_menu_game.Text = "";
            main_menu_game.Dock = DockStyle.Fill;
            main_menu_game.BackgroundImage = new Bitmap("bgKuning.png");
        }
        public void change_to_select_player()
        {
            main_menu_game.Visible = false;
            menu_select_player.Visible = false;
            menu_select_player.Visible = true;
            menu_select_player.Location = new Point(0, 0);
            menu_select_player.BackColor = Color.Black;
            menu_select_player.WindowState = FormWindowState.Maximized;
            menu_select_player.ControlBox = false;
            menu_select_player.MaximizeBox = false;
            menu_select_player.MinimizeBox = false;
            menu_select_player.ShowIcon = false;
            menu_select_player.Text = "";
            menu_select_player.Dock = DockStyle.Fill;
            menu_select_player.BackgroundImage = new Bitmap("bgKuning.png");
        }
        public void change_to_select_player_load()
        {
            main_menu_game.Visible = false;
            menu_select_player_load.Visible = false;
            menu_select_player_load.Visible = true;
            menu_select_player_load.Location = new Point(0, 0);
            menu_select_player_load.BackColor = Color.Black;
            menu_select_player_load.WindowState = FormWindowState.Maximized;
            menu_select_player_load.ControlBox = false;
            menu_select_player_load.MaximizeBox = false;
            menu_select_player_load.MinimizeBox = false;
            menu_select_player_load.ShowIcon = false;
            menu_select_player_load.Text = "";
            menu_select_player_load.Dock = DockStyle.Fill;
            menu_select_player_load.BackgroundImage = new Bitmap("bgKuning.png");
        }
        public void change_to_menu_game()
        {
            if (Map!=null)
            {
                Map.Close();
            }
            if (menu_shop_game != null)
            {
                menu_shop_game.Close();
            }
            player.Play();
            menu_select_player.Visible = false;
            menu_main_game.Visible = false;
            menu_main_game.Visible = true;
            menu_main_game.Location = new Point(0, 0);
            menu_main_game.BackColor = Color.Black;
            menu_main_game.WindowState = FormWindowState.Maximized;
            menu_main_game.ControlBox = false;
            menu_main_game.MaximizeBox = false;
            menu_main_game.MinimizeBox = false;
            menu_main_game.ShowIcon = false;
            menu_main_game.Text = "";
            menu_main_game.Dock = DockStyle.Fill;
            menu_main_game.BackgroundImage = new Bitmap("bgKuning.png");
        }
        public void change_to_choose_map()
        {
            first_time_play = 1;
            menu_main_game.Visible = false;
            menu_select_map.Visible = false;
            menu_select_map.Visible = true;
            menu_select_map.Location = new Point(0, 0);
            menu_select_map.BackColor = Color.Black;
            menu_select_map.WindowState = FormWindowState.Maximized;
            menu_select_map.ControlBox = false;
            menu_select_map.MaximizeBox = false;
            menu_select_map.MinimizeBox = false;
            menu_select_map.ShowIcon = false;
            menu_select_map.Text = "";
            menu_select_map.Dock = DockStyle.Fill;
            menu_select_map.BackgroundImage = new Bitmap("bgKuning.png");
        }
        public void change_to_map()
        {
            player.Stop();
            menu_select_map.Visible = false;
            Map = new map();
            Map.MdiParent = this;
            Map.Show();
            Map.Visible = false;
            Map.Visible = true;
            Map.Location = new Point(0, 0);
            Map.BackColor = Color.Black;
            Map.WindowState = FormWindowState.Maximized;
            Map.ControlBox = false;
            Map.MaximizeBox = false;
            Map.MinimizeBox = false;
            Map.ShowIcon = false;
            Map.Text = "";
            Map.Dock = DockStyle.Fill;
            Map.BackgroundImage = new Bitmap("bgKuning.png");
        }
        public void simpan_map(string[,] baru)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("list-map");
            xmlDoc.AppendChild(rootNode);
            arena.Clear();
            arena.Add(baru);
            for (int i = 0; i < arena.Count; i++)
            {
                XmlNode arenaNode = xmlDoc.CreateElement("arena");
                XmlAttribute idAttr = xmlDoc.CreateAttribute("id");
                idAttr.Value = i.ToString();
                arenaNode.Attributes.Append(idAttr);
                rootNode.AppendChild(arenaNode);
                arenaNode.InnerText = "";
                for (int j = 0; j < 15; j++)
                {
                    for (int k = 0; k < 20; k++)
                    {
                        arenaNode.InnerText += arena[i][j, k];
                    }
                    arenaNode.InnerText += "|";
                }

            }
            xmlDoc.Save(Application.StartupPath + "/arena.xml");
        }
        public void load_map()
        {
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(Application.StartupPath + "/arena.xml"))
            {
                arena.Clear();
                xmlDoc.Load(Application.StartupPath + "/arena.xml");
                XmlNode listNode = xmlDoc.DocumentElement;
                if (listNode.Name == "list-map")
                {

                    XmlNodeList listMap = listNode.ChildNodes;
                    for (int i = 0; i < listMap.Count; i++)
                    {
                        XmlNode mapNode = listMap[i];
                        string[,] temp = new string[15, 20];
                        int a = 0;
                        int z = 0;
                        for (int j = 0; j < mapNode.InnerText.Length; j++)
                        {
                            if (mapNode.InnerText[j] == '|')
                            {
                                a++;
                                z = 0;
                            }
                            else
                            {
                                temp[a, z] = mapNode.InnerText[j].ToString();
                                z++;
                            }
                        }
                        arena.Add(temp);
                        for (int j = 0; j < 15; j++)
                        {
                            for (int k = 0; k < 20; k++)
                            {
                                Console.Write(temp[j, k]);
                            }
                            Console.WriteLine();
                        }
                    }
                }
            }
        }
        public void exit_game()
        {
            this.Close();
        }
    }
}
