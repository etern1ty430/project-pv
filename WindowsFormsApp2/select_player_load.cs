﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class select_player_load : Form
    {
        public int chooseplayer = -1;
        public select_player_load()
        {
            InitializeComponent();
        }

        private void select_player_load_Load(object sender, EventArgs e)
        {
            this.Focus();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //create_player.BackgroundImage = new Bitmap("");
            this.player1.BackgroundImage = new Bitmap("hitam.png");
            this.player1.BackColor = Color.Transparent;
            this.player1.BackgroundImageLayout = ImageLayout.Stretch;
            this.player1.FlatStyle = FlatStyle.Flat;

            this.player2.BackgroundImage = new Bitmap("hijau.png");
            this.player2.BackColor = Color.Transparent;
            this.player2.BackgroundImageLayout = ImageLayout.Stretch;
            this.player2.FlatStyle = FlatStyle.Flat;

            this.player3.BackgroundImage = new Bitmap("kuning.png");
            this.player3.BackColor = Color.Transparent;
            this.player3.BackgroundImageLayout = ImageLayout.Stretch;
            this.player3.FlatStyle = FlatStyle.Flat;

            this.player4.BackgroundImage = new Bitmap("putih.png");
            this.player4.BackColor = Color.Transparent;
            this.player4.BackgroundImageLayout = ImageLayout.Stretch;
            this.player4.FlatStyle = FlatStyle.Flat;


            this.create_player.BackgroundImage = new Bitmap("load.png");
            this.create_player.BackColor = Color.Transparent;
            this.create_player.BackgroundImageLayout = ImageLayout.Stretch;
            this.create_player.FlatStyle = FlatStyle.Flat;

            this.back.BackgroundImage = new Bitmap("buttonback.png");
            this.back.BackColor = Color.Transparent;
            this.back.BackgroundImageLayout = ImageLayout.Stretch;
            this.back.FlatStyle = FlatStyle.Flat;

            this.display_player.FlatStyle = FlatStyle.Flat;
            this.display_player.BackColor = Color.DimGray;
            this.BackColor = Color.BlanchedAlmond;
        }

        private void player1_Click(object sender, EventArgs e)
        {
            this.display_player.BackgroundImage = new Bitmap("hitam1.png");
            this.display_player.BackgroundImageLayout = ImageLayout.Stretch;
            chooseplayer = 0;
            this.player1.BackColor = Color.DarkGray;
            this.player2.BackColor = Color.Transparent;
            this.player3.BackColor = Color.Transparent;
            this.player4.BackColor = Color.Transparent;
        }

        private void player2_Click(object sender, EventArgs e)
        {
            this.display_player.BackgroundImage = new Bitmap("hijau1.png");
            this.display_player.BackgroundImageLayout = ImageLayout.Stretch;
            chooseplayer = 1;
            this.player1.BackColor = Color.Transparent;
            this.player2.BackColor = Color.DarkGray;
            this.player3.BackColor = Color.Transparent;
            this.player4.BackColor = Color.Transparent;
        }

        private void player3_Click(object sender, EventArgs e)
        {
            this.display_player.BackgroundImage = new Bitmap("kuning1.png");
            this.display_player.BackgroundImageLayout = ImageLayout.Stretch;
            chooseplayer = 2;
            this.player1.BackColor = Color.Transparent;
            this.player2.BackColor = Color.Transparent;
            this.player3.BackColor = Color.DarkGray;
            this.player4.BackColor = Color.Transparent;
        }

        private void player4_Click(object sender, EventArgs e)
        {
            this.display_player.BackgroundImage = new Bitmap("putih1.png");
            this.display_player.BackgroundImageLayout = ImageLayout.Stretch;
            chooseplayer = 3;
            this.player1.BackColor = Color.Transparent;
            this.player2.BackColor = Color.Transparent;
            this.player3.BackColor = Color.Transparent;
            this.player4.BackColor = Color.DarkGray;
        }

        private void create_player_Click(object sender, EventArgs e)
        {
            if (chooseplayer == -1)
            {
                MessageBox.Show("YOU NOT CHOOSE CHARACTER YET!");
            }
            else
            {
                ((Form1)MdiParent).menu_select_player.chooseplayer = chooseplayer;
                ((Form1)MdiParent).change_to_menu_game();
                ((Form1)MdiParent).first_time_play = 1;
            }
        }

        private void back_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_mainmenu();
        }
    }
}
