﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class menugame : Form
    {
        public menugame()
        {
            InitializeComponent();
        }

        private void menugame_Load(object sender, EventArgs e)
        {
            this.Focus();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //label title
            this.title.BackgroundImage = new Bitmap("TITLE.png");
            this.title.BackgroundImageLayout = ImageLayout.Stretch;
            this.title.BackColor = Color.Black;
            this.title.FlatStyle = FlatStyle.Flat;
            //button play
            this.playbutton.BackgroundImage = new Bitmap("playgame.png");
            this.playbutton.BackgroundImageLayout = ImageLayout.Stretch;
            this.playbutton.BackColor = Color.Transparent;
            this.playbutton.FlatStyle = FlatStyle.Flat;
            //button back to main menu
            this.exit.BackgroundImage = new Bitmap("buttonbacktomainmenu.png");
            this.exit.BackgroundImageLayout = ImageLayout.Stretch;
            this.exit.BackColor = Color.Transparent;
            this.exit.FlatStyle = FlatStyle.Flat;
            //button back to main menu
            this.shop.BackgroundImage = new Bitmap("buttonshop.png");
            this.shop.BackgroundImageLayout = ImageLayout.Stretch;
            this.shop.BackColor = Color.Transparent;
            this.shop.FlatStyle = FlatStyle.Flat;
        }

        private void playbutton_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_choose_map();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_mainmenu();
        }

        private void shop_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_shop();
        }
    }
}
